import os
import re
import sys
import time
import logger
import xlsxwriter


code_list = ['POSICION ARANCELARIA', 'NCM NALADI HS CODE', 'NCMCODE', 'NCM CODE NO', 'NCM CODES', 'NCM CODE IS', 'NCM CODE IN BRAZIL', 'NCM OF THE ITEMS',
             'NCM CODE HS CODE', 'NCM NO HS CODE', 'NCM CODES D', 'NCM CODE NUMBER', 'NCM CODE', 'NCM NR', 'NCM HS CODE', 'NCM CODE NUMBER', 'NCM IS', 'NCM FOR PRODUCT', 'NCM FOR METAL CRATES',
             'NCM NUMBERS', 'NCM NUMBER', 'NCM NOS', 'NCM N0', 'NCM NO', 'NCM HTS', 'NCMS CODE','NCMSS', 'NCMS', 'NCM SH', 'NCM HS', 'NCM N', 'NCM',
             'NALADI SH', 'NALADI', 'HS NO', 'HS CODE N', 'HSCODES NO', 'HS CODES', 'HSCODE NO', 'HSCODENO', 'HSCODE', 'HS CODE', 'HS NUMBER', 'COMMODITY', ]

code_list2 = ['NCM CODE', 'NCM NR', 'NCM HS CODE', 'NCM NUMBER', 'NCM NO', 'NCM HTS', 'NCMS', 'NCM HS', 'NCM', ]

pkg_info_dict = {'BALES': 'BLS', 'BARRELS': 'BRL', 'BARS': 'BAR', 'FIBREBOARD': 'BXS', 'BEAMS': 'BMS', 'BIDONS': 'BDN', 'BBAGS': 'BBG', 'BIG CARTONS': 'BGC',
                 'CAIXAS': 'BXS', 'OCTABIN': 'BIN', 'BINS': 'BIN', 'BLOCKS': 'BLK', 'BOBBINS': 'BOB', 'BOTTELON': 'BOT', 'BOTTLES': 'BTL', 'BOXES': 'BXS', 'BUCKETS': 'BKT',
                 'BULK': 'BKC', 'BULTOS': 'BUL', 'BUNDLES': 'BDL', 'CAGE': 'CAG', 'CANS': 'CA', 'CARTONS': 'CTN', 'CASES': 'CS',
                 'CASKS': 'CSK', 'COILS': 'CLS', 'COLLI': 'COL', 'CRADLES': 'CDL', 'CRATES': 'CRT', 'CYLINDERS': 'CYL', 'STEEL': 'DRS', 'DRUMS': 'DRS',
                 'FLASKS': 'FLK', 'FLATS': 'FLT', 'FLEXIBAGS': 'FLB', 'GALLONS': 'GAL', 'GAYLDS': 'GAY', 'IBCS': 'IBC', 'JARS': 'JAR',
                 'JUGS': 'JUG', 'KEGS': 'KGS', 'LIFT VAN': 'LVN', 'LIFTS': 'LFT', 'LITERS': 'LTR', 'LOTS': 'LOT', 'PACKS': 'PKG',
                 'PAILS': 'PLS', 'PAIRS': 'PRS', 'WOODEN': 'PLT', 'PLASTIC': 'PLT', 'PALETTES': 'PLT', 'PALLETS': 'PLT', 'PARCELS': 'PCL', 'PIECES': 'PCS', 'PLAST./BAG': 'PLB',
                 'PACKAGES': 'PKG', 'PKG': 'PKG', 'PILES': 'PLS', 'PLATES': 'PLA', 'POLIBAGS': 'PBG', 'RACKS': 'RCK', 'REELS': 'RLS',
                 'ROLLS': 'RLL', 'SACKS': 'SAC', 'PALLET': 'PLT', 'CONTAINER': 'CNT', 'SETS': 'SET', 'SHEETS': 'SHT', 'SKIDS': 'SKD',
                 'SLABS': 'SLB', 'SLING BAG': 'SBG', 'SPOOL': 'SPL', 'TANK': 'THK', 'TIERCES': 'TRC', 'TINS': 'TIN', 'TOTE': 'TOT',
                 'TRAYS': 'TRA', 'UNITS': 'UNT', 'UNKNOWN': 'UNK', 'VAN': 'VAN', 'VOLUMES': 'VOL', 'WOODEN BIN': 'WBI',
                 'BAGS': 'BGS', 'BALE': 'BLS', 'BARREL': 'BRL', 'BAR': 'BAR', 'BEAM': 'BMS', 'BIDON': 'BDN', 'BIG BAG': 'BBG',
                 'BIG CARTON': 'BGC', 'BIN': 'BIN', 'BLOCK': 'BLK', 'BOBBIN': 'BOB', 'BOTTLE': 'BTL', 'BOX': 'BXS', 'BUCKET': 'BKT',
                 'CAN': 'CAN', 'CARTON': 'CTN', 'CASE': 'CS', 'CASK': 'CSK', 'COIL': 'CLS', 'CRADLE': 'CDL', 'CRATE': 'CRT',
                 'CYLINDER': 'CYL', 'DRUM': 'DRS', 'FLASK': 'FLK', 'FLAT': 'FLT', 'FLEXIBAG': 'FLB', 'GALLON': 'GAL', 'GAYLD': 'GAY',
                 'IBC': 'IBC', 'JAR': 'JAR', 'JUG': 'JUG', 'KEG': 'KGS', 'LIFT': 'LFT', 'LITER': 'LTR', 'LOT': 'LOT', 'PACKAGE': 'PKG',
                 'PACK': 'PKG', 'PAIL': 'PLS', 'PAIR': 'PRS', 'PARCEL': 'PCL', 'PIECE': 'PCS', 'PILE': 'PLS', 'PLATE': 'PLA',
                 'POLIBAG': 'PBG', 'RACK': 'RCK', 'REEL': 'RLS', 'ROLL': 'RLL', 'SACK': 'SAC', 'SET': 'SET', 'SHEET': 'SHT',
                 'SKID': 'SKD', 'SLAB': 'SLB', 'TIERCE': 'TRC', 'TIN': 'TIN', 'TRAY': 'TRA', 'UNIT': 'UNT',
                 'VOLUME': 'VOL', 'BAG': 'BGS', 'BUNDLE': 'BDL'}

package_list = ['BALE', 'BARREL', 'BAR', 'BEAM', 'BIDON', 'BIG BAG', 'BIG CARTON', 'BIN', 'BLOCK', 'BOBBIN', 'BOTTLE',
                'BOX', 'BUCKET', 'CAN', 'CARTON', 'CASE', 'CASK', 'COIL', 'CRADLE', 'CRATE', 'CYLINDER', 'DRUM', 'FLASK',
                'FLAT', 'FLEXIBAG', 'GALLON', 'GAYLD', 'IBC', 'JAR', 'JUG', 'KEG', 'LIFT', 'LITER', 'LOT', 'PACKAGE',
                'PACK', 'PAIL', 'PAIR', 'PARCEL', 'PIECE', 'PILE', 'PLATE', 'POLIBAG', 'RACK', 'REEL', 'TRAY',
                'ROLL', 'SACK', 'SET', 'SHEET', 'SKID', 'SLAB', 'TIERCE', 'TIN', 'TUBE', 'UNIT', 'VOLUME', 'BAG']


def desc(string):
    split_code = " "
    seq_list = []
    weight_seq_list, pkg_seq_list = [], []
    final_ncm_list, return_ncm_list = [], []
    ncm_simple, ncm_var, ncm_sub = 0, 0, 0
    new_string = string.replace(' s ', '').replace('AUTOMOTIVE PARTS', '').replace('.', '').replace(',', ' ')\
        .replace('AND', '').replace('WOODEN', '').replace('   ', ' ').replace('  ', ' ')
    try:
        if new_string.__contains__('KGM GROSS'):
            list_sequences = new_string.strip(" ").split("KGM GROSS")[:-1]
            for i in range(0, len(list_sequences)):
                for code in code_list:
                    if code in list_sequences[i]:
                        if list_sequences[i].count(code) == 1:
                            return_ncm_list = fetching_more_ncm(i, code, list_sequences)
                            split_code = code
                            break
                        elif list_sequences[i].count(code) > 1:
                            return_ncm_list = fetching_more_ncm(i, code, list_sequences)
                            split_code = code
                            break
                        break
                if return_ncm_list:
                    final_ncm_list.append(return_ncm_list)
            list_sequences = list_sequences[:-1]
            for lis in list_sequences:
                lis = str(lis) + 'KGM GROSS'
                weight_seq_list.append(lis)
                if split_code in str(lis):
                    lis = lis.split(split_code)[1]
                pkg_seq_list.append(lis.strip(' '))
        # if string does not have SHIPPER LOAD
        else:
            for code in code_list:
                if str(new_string).__contains__(code):
                    split_code = code
                    seq_list = new_string.split(code)[1:]
                    weight_seq_list = new_string
                    pkg_seq_list = new_string
                    break
            final_ncm_list = capturing_ncm_list(seq_list)
        if not final_ncm_list:
            split_code = 'COMMODITY'
            seq_list = new_string.split(split_code)
            final_ncm_list = capturing_ncm_list(seq_list)
        ncm_list = final_ncm_list
        ncm_list = hs8conversion(ncm_list)
        if len(ncm_list) == 0:
            return 6, split_code, [], []
        counter = 0
        updated_ncm_list = []
        if len(ncm_list) == 1:
            if str(ncm_list).__contains__(' '):
                new_ncm_list = str(ncm_list).strip("['").strip("']").split(' ')
                for i in range(0, len(new_ncm_list) - 1):
                    if new_ncm_list[i] == new_ncm_list[i+1] or new_ncm_list[i] in new_ncm_list[i+1] or new_ncm_list[i+1] in new_ncm_list[i]:
                        counter += 1
                if counter == len(new_ncm_list) - 1:
                    updated_ncm_list.append(new_ncm_list[0])
                    ncm_list = updated_ncm_list
                    return 1, split_code, ncm_list, pkg_seq_list
                return 1, split_code, ncm_list, pkg_seq_list
            return 1, split_code, ncm_list, pkg_seq_list
        updated_ncm_list = []
        if len(ncm_list) > 1:
            i, count = 0, 0
            if len(ncm_list) == 2:
                if ncm_list[i].__contains__(' '):
                    new_ncm1 = ncm_list[i].split(' ')
                    if len(new_ncm1) > 1:
                        for j in range(0, len(new_ncm1) - 1):
                            if new_ncm1[j] == new_ncm1[j + 1] or new_ncm1[j][:6] in new_ncm1[j + 1][:6]:
                                updated_ncm_list.append(ncm_list[j][:6])
                            else:
                                break
                if len(updated_ncm_list) == 2:
                    ncm_list = []
                    if updated_ncm_list[0] == updated_ncm_list[1]:
                        ncm_list.append(updated_ncm_list[0])
                        return 1, split_code, ncm_list, pkg_seq_list
                updated_ncm_list = []
                if ncm_list[i] in ncm_list[i + 1]:
                    updated_ncm_list.append(ncm_list[i+1])
                    ncm_list = updated_ncm_list
                    return 1, split_code, ncm_list, pkg_seq_list
                elif ncm_list[i+1] in ncm_list[i]:
                    updated_ncm_list.append(ncm_list[i])
                    ncm_list = updated_ncm_list
                    return 1, split_code, ncm_list, pkg_seq_list
                elif ncm_list[i][:6] in ncm_list[i + 1]:
                    updated_ncm_list.append(ncm_list[i+1])
                    ncm_list = updated_ncm_list
                    return 1, split_code, ncm_list, pkg_seq_list
                elif ncm_list[i + 1][:6] in ncm_list[i]:
                    updated_ncm_list.append(ncm_list[i])
                    ncm_list = updated_ncm_list
                    return 1, split_code, ncm_list, pkg_seq_list
            while i < len(ncm_list) - 1:         # to check directly for simple
                if len(ncm_list[i]) == len(ncm_list[i+1]):
                    if ncm_list[i] in ncm_list[i+1] or ncm_list[i+1] in ncm_list[i]:
                        count += 1
                        i += 1
                        continue
                    elif ncm_list[i].__contains__(' '):
                        break
                    elif ncm_list[i][:6] in ncm_list[i+1][:6] or ncm_list[i+1][:6] in ncm_list[i][:6]:
                        if not str(updated_ncm_list).__contains__(ncm_list[i][:6]):
                            new_ncm1 = ncm_list[i].split(' ')
                            if len(new_ncm1) > 1:
                                for i in range(0, len(new_ncm1) - 1):
                                    if new_ncm1[i] == new_ncm1[i + 1] or new_ncm1[i][:6] in new_ncm1[i + 1][:6]:
                                        updated_ncm_list.append(ncm_list[i][:6])
                                    else:
                                        count += 1
                                        i += 1
                                        break
                            else:
                                updated_ncm_list.append(ncm_list[i])
                        count += 1
                        i += 1
                        continue
                    else:
                        break
                else:
                    break
            if count == len(ncm_list)-1:
                if updated_ncm_list:
                    ncm_list = updated_ncm_list
                return 1, split_code, ncm_list, pkg_seq_list
        loop = 0
        while loop < 1:
            i = 0
            while i < len(ncm_list) - 1:
                var, sub = 0, 0
                ncmlisti = ncm_list[i].split(" ")
                ncmlistj = ncm_list[i + 1].split(" ")
                if len(ncmlisti) == len(ncmlistj):
                    if len(ncmlisti) == 1 and len(ncmlistj) == 1:
                        if ncm_list[i] in ncm_list[i + 1] or ncm_list[i + 1] in ncm_list[i]:
                            i = i + 1
                            ncm_simple += 1
                        else:
                            i = i + 1
                            sub += 1
                    elif len(ncmlisti) > 1 and len(ncmlistj) > 1:
                        var, sub = diff_all(ncmlisti, ncmlistj)  ########   Differentiate various and sub
                        i = i + 1
                elif not len(ncmlisti) == len(ncmlistj):
                    var, sub = diff_all(ncm_list[i], ncm_list[i + 1])  ########   Differentiate various and sub
                    i = i + 1
                ncm_var += var
                ncm_sub += sub
            loop += 1
        if ncm_sub > 0 and ncm_var > 0:
            return 3, split_code, ncm_list, pkg_seq_list  # various and sub
        elif ncm_simple > 0 and ncm_var > 0:
            return 4, split_code, ncm_list, pkg_seq_list  # simple and various
        elif ncm_simple > 0 and ncm_sub > 0:
            weight_list, cbm_list = fetching_weight_cbm(weight_seq_list)
            if not len(weight_list) == len(ncm_list) and not len(weight_list) == len(ncm_list) + 1:
                return 2, split_code, ncm_list, pkg_seq_list  # various
            return 5, split_code, ncm_list, pkg_seq_list  # sub and simple
        elif ncm_simple == len(ncm_list) - 1 and ncm_var == 0 and ncm_sub == 0:
            return 1, split_code, ncm_list, pkg_seq_list  # simple
        elif ncm_var == len(ncm_list) - 1 and ncm_simple == 0 and ncm_sub == 0:
            return 2, split_code, ncm_list, pkg_seq_list  # various
        elif ncm_sub == len(ncm_list) - 1 and ncm_var == 0 and ncm_simple == 0:
            return 3, split_code, ncm_list, pkg_seq_list  # sub
    except:
        return 6, " ", [], []


def hs8conversion(uncoded_ncm_list):
    coded_ncm_list = []
    for new_ncm in uncoded_ncm_list:
        if new_ncm.__contains__(' '):
            new_ncm_list = str(new_ncm).split(' ')
            new_ncm = ''
            for ncm in new_ncm_list:
                if len(ncm) == 1 or len(ncm) == 2:
                    continue
                elif len(ncm) > 8:
                    ncm = ncm[:8]
                while len(ncm) < 8:
                    ncm = ncm + '0'
                if ncm == '00000000':
                    continue
                new_ncm += ncm + ' '
            coded_ncm_list.append(new_ncm.strip(' '))
        else:
            if len(new_ncm) > 8:
                new_ncm = new_ncm[:8]
            while len(new_ncm) < 8:
                new_ncm = new_ncm + '0'
            coded_ncm_list.append(new_ncm.strip(' '))
    return coded_ncm_list


def fetching_more_ncm(i, code, list_sequences):
    seq_list = []
    chunkify = list_sequences[i].split(code)[1:]
    j = 0
    while j < len(chunkify):
        for code in code_list2:
            if str(chunkify[j]).__contains__(code):
                code_seq = str(chunkify[j]).split(code)
                if code_seq:
                    for code_seq1 in code_seq:
                        new_code_seq = code_seq1.replace('  ', ' ')
                        if new_code_seq != '':
                            seq_list.append(new_code_seq.strip(' '))
                    break
        j += 1
    if not seq_list:
        j = 0
        while j < len(chunkify):
            code_seq = chunkify[j]
            if code_seq != '':
                new_code_seq = code_seq.replace('  ', ' ')
                seq_list.append(new_code_seq.strip(' '))
            j += 1
    ncm_list = capturing_ncm_list(seq_list)
    new_ncm_list = ' '.join(ncm for ncm in ncm_list)
    return new_ncm_list


def capturing_ncm_list(seq_list):
    ncm_list = []
    remove_list = ['MTQ GROSS', 'FTQ GROSS', 'CBM', 'KGM NET', 'KGM', 'KG NET', 'KG']
    for seq1 in seq_list:
        seq1 = re.sub(r' 0+ ', ' ', seq1).replace('CODE ', '')
        for rem_val in remove_list:
            if rem_val in seq1:
                try:
                    new_match = re.search(r'( \d+ '+rem_val+')', seq1.strip(' ')).group()
                except:
                    try:
                        new_match = re.search(r'(\d+ '+rem_val+')', seq1.strip(' ')).group()
                    except:
                        new_match = ''
                seq1 = seq1.replace(new_match, '')
            else:
                continue
        if seq1 == '':
            continue
        ncm_token = '0000'
        if isinstance(seq1, str):
            if len(seq_list) == 1:
                match = re.search(r'([A-Za-z@]+)', seq1.strip(' '))
                if match:
                    ncm_token = seq1.split(match.group())[0].strip(' ')
                    if ncm_token != " ":
                        if re.search(r'(^\d{2} \d{2})', ncm_token):
                            ncm_token = re.sub(r'( )', '', ncm_token)
                        else:
                            ncm_token = re.sub(r'( \d{1,3}$)', '', ncm_token)
                        if ncm_token != '':
                            ncm_list.append(ncm_token.strip(' '))
                else:
                    ncm_token = re.search('[0-9 ]+', seq1.strip(" ")).group()
                    if ncm_token != '':
                        ncm_list.append(ncm_token)
            else:
                match = re.search(r'([A-Za-z@]+)', seq1.strip(' '))
                if match:
                    ncm_token = seq1.split(match.group())[0].strip(' ')
                    if ncm_token != " ":
                        if re.search(r'(^\d{2} \d{2})', ncm_token):
                            ncm_token = re.sub(r'( )', '', ncm_token)
                        else:
                            ncm_token = re.sub(r'( \d{1,3}$)', '', ncm_token)
                        if ncm_token != '':
                            ncm_list.append(ncm_token.strip(' '))
                else:
                    ncm_token = re.search('[0-9 ]+', seq1.strip(" ")).group()
                    if ncm_token != '':
                        ncm_list.append(ncm_token)
        else:
            token_list = []
            new_ncm_token = ' '
            for code_seq in seq1:
                if code_seq != '':
                    match = re.search(r'([A-Za-z@]+)', code_seq.strip(' '))
                    if match:
                        ncm_token = code_seq.split(match.group())[0].strip(' ')
                        if ncm_token != " ":
                            if re.search(r'(\d{2} \d{2})', ncm_token):
                                ncm_token = re.sub(r'( )', '', ncm_token)
                            else:
                                ncm_token = re.sub(r'( \d{1,3}$)', '', ncm_token)
                            if ncm_token != '':
                                ncm_list.append(ncm_token.strip(' '))
                    else:
                        ncm_token = re.search('[0-9 ]+', code_seq.strip(" ")).group()
                if ncm_token not in token_list:
                    token_list.append(ncm_token)
                    new_ncm_token += ' ' + ncm_token
            if ncm_token != '':
                ncm_list.append(new_ncm_token.replace('  ', ' ').strip(' ').strip(','))
    return ncm_list


def diff_all(ncm_i, ncm_j):
    count = 0
    ncm_var, ncm_sub = 0, 0
    try:
        new_i = ncm_i.split(" ")
        new_j = ncm_j.split(" ")
    except:
        new_i = ncm_i
        new_j = ncm_j
    try:
        for na, nb in zip(new_i, new_j):
            if nb in ncm_i or na in ncm_j:
                count += 1
        if count > 0:
            ncm_var += 1
        else:
            ncm_sub += 1
    except:
        return 0, 0, 0
    return ncm_var, ncm_sub


def fetching_pkg_qnty(pkg_seq_list):
    pkg_info_list, qnty_info_list = [], []
    try:
        for pkg_seq2 in pkg_seq_list:
            pkg_info, qnty_info = ' ', ' '
            pkg_seq = pkg_seq2.split(' ')
            for pkg_seq1 in pkg_seq:
                try:
                    for pkg_key, pkg_value in pkg_info_dict.items():
                        if str(pkg_seq1).upper() == pkg_key.upper():
                            pkg_info = pkg_value
                            qnty = re.search(r'( \d+ '+pkg_seq1+')', pkg_seq2)
                            qnty_info = int(qnty.group().strip(' ').split(" ")[0])

                except:
                    if str(pkg_seq1).__contains__('WEIGHT VOLUME'):
                        pkg_seq1 = str(pkg_seq1).split('WEIGHT VOLUME')[1].strip(' ')
                        seq = pkg_seq1.split('KGM GROSS')[0].strip(' ').split(' ')
                        for s1 in seq:
                            for pkg_key, pkg_value in pkg_info_dict.items():
                                if str(s1).upper() == pkg_key.upper():
                                    pkg_info = pkg_value
                                    qnty = re.search(r'( \d+ '+s1+')', pkg_seq2)
                                    qnty_info = int(qnty.group().strip(' ').split(" ")[0])

                    if pkg_info != ' ':
                        break
            pkg_info_list.append(pkg_info)
            qnty_info_list.append(qnty_info)
        # #logger.log.debug("pkg_qnty: pkg and qnty info obtained")
    except Exception as ex:
        logger.log.error("pkg_qnty: While fetching pkg or qnty info", ex)
    return pkg_info_list, qnty_info_list


def fetching_weight_cbm(string):
    # #logger.log.debug("Fetching weight List")
    weight_list, cbm_list = [], []
    weight, new_cbm = ' ', ' '
    # #logger.log.debug("sub_Items: Inside Sub item")
    for string1 in string:
        try:
            try:
                pattern1 = "(.*)(?=KGM GROSS)"
                match1 = re.search(pattern1, string1)
                if str(match1) == 'None':
                    continue
                elif match1:
                    weight = (match1.group()).strip(" ").split(" ")[-1]
            except:
                weight = ' '
            weight_list.append(weight)
            try:
                if str(string1).__contains__("MTQ GROSS"):
                    new_cbm = str(string1).split("MTQ GROSS")[0].strip(" ").split(' ')[-1]
                elif str(string1).__contains__("FTQ GROSS"):
                    new_cbm = str(string1).split("FTQ GROSS")[0].strip(" ").split(' ')[-1]
                else:
                    new_cbm = str(string1).split("CBM")[0].strip(" ").split(' ')[-1]
            except:
                new_cbm = ' '
            cbm_list.append(new_cbm)
        except Exception as ex:
            logger.log.error("sub_Items: While capturing Subitem Commodity", ex)
    return weight_list, cbm_list


def hapag_search(input_db, input_file):
    input = xlsxwriter.Workbook(input_file)
    sheet = input.add_worksheet()

    row1 = 0
    sheet.write(row1, 0, "BL")
    sheet.write(row1, 1, "NCM")
    sheet.write(row1, 2, "WEIGHT")
    sheet.write(row1, 3, "CBM")
    sheet.write(row1, 4, "PACKAGE")
    sheet.write(row1, 5, "QUANTITY")
    row1 += 1

    sequence_split = 'KGM GROSS'
    count = 0
    logger.log.debug("Total length of DB = " + str(len(input_db)) + " ")
    for line in input_db:
        try:
            count += 1
            bl_string = str(line[0])
            #logger.log.debug(str(count) + " Started " + bl_string + " ")
            if bl_string == '':
                continue
            string_value = str(line[1]).upper()
            special_char_list1 = ['+', '/', '\\r\\n', '\r\n\r\n', '\r\n', "'", '"', '*', '(', ')', '[', ']',  ';', ':', '-', '#', '&',
                                  '_', '?', '@', '>', '<', "'S", ' S ', ' X ', '   ', '  ', 'WOODEN', ' W ', '   ']

            for k in special_char_list1:
                string_value = string_value.replace(k, ' ')

            special_char_list2 = ['FLAT COLL SLAC', 'OPEN TOP', 'HIGH CUBE', 'GENERAL PU', '�', '+', '|', '=', '%', 'SHIPPER DECLARES PACKAGE',
                                  'WOODEN PACKAGES TREATED AND CERTIFIED', 'WOODEN PACKAGE', 'PROCESSED AND']
            for k in special_char_list2:
                string_value = string_value.replace(k, '')
            if string_value.__contains__('E M P T Y EQUIPMENT') or string_value.__contains__('TANK EMPTY CLEAN WITHOUT RESIDUES'):
                sheet.write(row1, 0, bl_string)
                sheet.write(row1, 1, 'EMPTY EQUIPMENT')
                sheet.write(row1, 2, str(['86090000']))
                row1 += 1
                #logger.log.debug("hapag: Empty Equipment")
                continue

            # #logger.log.debug("hapag: String replacement completed  @ level 1")
            try:
                # capturing ncm list and type of BL
                return_value, code, ncm_list, pkg_seq_list = desc(string_value)
            except TypeError as ex:
                logger.log.error("hapag: Type Error in NCM capture", ex)
                continue

            ncm_list = hs8conversion(ncm_list)
            # splitting as sequences and further with return code
            string1 = []
            str_wei_cbm = string_value.replace(',', '').split('MTQ GROSS')[:-2]
            new_string = string_value.replace(',', '').split(sequence_split)[1:]
            if return_value == 1:
                if ncm_list == "['86090000']":
                    sheet.write(row1, 0, bl_string)
                    sheet.write(row1, 1, str(ncm_list))
                    #logger.log.debug("hapag: Simple NCM captured")
                    row1 += 1
                    continue
                sheet.write(row1, 0, bl_string)
                ncm_list1 = ncm_list[0].split(' ')
                if len(ncm_list1) > 1:
                    i = 0
                    while i < len(ncm_list1) - 1:
                        if ncm_list1[i] == ncm_list1[i+1] or ncm_list1[i][:6] == ncm_list1[i+1][:6]:
                            i += 1
                        else:
                            break
                    if i == len(ncm_list1) - 1:
                        sheet.write(row1, 1, str(ncm_list1[0]))
                        row1 += 1
                    else:
                        sheet.write(row1, 1, str(ncm_list))
                        row1 += 1
                elif len(ncm_list) > 1:
                    try:
                        i = 1
                        ncm_list1 = [ncm_list[0]]
                        while i <= len(ncm_list):
                            if ncm_list[i] not in ncm_list[0] and ncm_list[0] not in ncm_list[i]:
                                ncm_list1.append(ncm_list[i])
                                sheet.write(row1, 1, str(ncm_list1))
                                row1 += 1
                            else:
                                sheet.write(row1, 1, str(ncm_list1))
                                row1 += 1
                            i += 1
                            break
                    except:
                        sheet.write(row1, 1, str(ncm_list))
                        row1 += 1
                else:
                    sheet.write(row1, 1, str(ncm_list))
                    row1 += 1
                #logger.log.debug("hapag: Simple NCM captured")

            elif return_value == 2 or return_value == 4:
                sheet.write(row1, 0, bl_string)
                sheet.write(row1, 1, str(ncm_list))
                row1 += 1
                #logger.log.debug("hapag: Various NCM captured")

            elif return_value == 3:
                new_wei_list = []
                pkg_list, qnty_list = fetching_pkg_qnty(pkg_seq_list)
                for new in new_string:
                    string1.append(new.split(code)[0])
                if not len(string1) > len(ncm_list):
                    for new in str_wei_cbm:
                        new_weight = new + 'MTQ GROSS'
                        new_wei_list.append(new_weight)
                else:
                    for new in str_wei_cbm:
                        new = new + 'MTQ GROSS'
                        new_wei_list.append(new)
                weight_list, cbm_list = fetching_weight_cbm(new_wei_list)
                if len(weight_list) > len(ncm_list):
                    weight_list = weight_list[:len(ncm_list)]
                if len(cbm_list) > len(ncm_list):
                    cbm_list = cbm_list[:len(ncm_list)]
                if len(pkg_list) > len(ncm_list):
                    pkg_list = pkg_list[:len(ncm_list)]
                if len(qnty_list) > len(ncm_list):
                    qnty_list = qnty_list[:len(ncm_list)]
                if len(ncm_list) > len(weight_list):
                    ncm_list = ncm_list[:len(weight_list)]
                for ncm, weight, cbm, pkg, qnty in zip(ncm_list, weight_list, cbm_list, pkg_list, qnty_list):
                    sheet.write(row1, 0, bl_string)
                    sheet.write(row1, 1, str(ncm))
                    sheet.write(row1, 2, str(weight))
                    sheet.write(row1, 3, str(cbm))
                    sheet.write(row1, 4, str(pkg))
                    sheet.write(row1, 5, str(qnty))
                    row1 += 1
                #logger.log.debug("hapag: Sub Item captured")

            elif return_value == 5:
                new_wei_list = []
                pkg_list, qnty_list = fetching_pkg_qnty(pkg_seq_list)
                for new in new_string:
                    string1.append(new.split(code)[0])
                if not len(string1) > len(ncm_list):
                    for new in str_wei_cbm:
                        new_weight = new + 'MTQ GROSS'
                        new_wei_list.append(new_weight)
                else:
                    for new in str_wei_cbm:
                        new = new + 'MTQ GROSS'
                        new_wei_list.append(new)
                weight_list, cbm_list = fetching_weight_cbm(new_wei_list)
                if len(weight_list) > len(ncm_list):
                    weight_list = weight_list[:len(ncm_list)]
                if len(cbm_list) > len(ncm_list):
                    cbm_list = cbm_list[:len(ncm_list)]
                if len(pkg_list) > len(ncm_list):
                    pkg_list = pkg_list[:len(ncm_list)]
                if len(qnty_list) > len(ncm_list):
                    qnty_list = qnty_list[:len(ncm_list)]
                if len(ncm_list) > len(weight_list):
                    ncm_list = ncm_list[:len(weight_list)]
                for ncm, weight, cbm, pkg, qnty in zip(ncm_list, weight_list, cbm_list,
                                                                  pkg_list, qnty_list):
                    sheet.write(row1, 0, bl_string)
                    sheet.write(row1, 1, str(ncm))
                    sheet.write(row1, 2, str(weight))
                    sheet.write(row1, 3, str(cbm))
                    sheet.write(row1, 4, str(pkg))
                    sheet.write(row1, 5, str(qnty))
                    row1 += 1
                #logger.log.debug("hapag: Simple and Sub Item")

            elif return_value == 6:
                code = 'HS CODE'
                seq1 = string_value.split(code)[1]
                match = re.search(r'([A-Za-z@]+)', seq1.strip(' '))
                ncm_token = seq1.split(match.group())[0].strip(' ')
                if ncm_token != " ":
                    ncm_list.append(ncm_token.strip(' ')[:8])
                ncm_list = hs8conversion(ncm_list)
                sheet.write(row1, 0, bl_string)
                sheet.write(row1, 1, str(ncm_list))
                row1 += 1
                #logger.log.debug("hapag: Unknown Type")

        except Exception as ex:
            logger.log.error("hapag: While fetching or updating sheet")

    input.close()
