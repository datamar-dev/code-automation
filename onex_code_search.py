import os
import re
import sys
import time
import logger
import xlsxwriter


code_list = ['NCM HS CODE', 'NCM CODES', 'NCM CODE NO', 'NCM CODE', 'NCM Code', 'NCM NR', 'NCM NUMBER', 'NCM NO', 'NCMNO', ' HTS ', 'NCMS NO', 'NCMs',
             'NCMS', 'NCM HS', 'NCM', ' CM NO ', ' M NO ', ' CM CODE ', ' CM ', 'HS CODE NO', 'HS CODE N', 'HSCODE',
              'HS NUMBER', 'HS CODES', 'HS CODE', 'HSCODE NO', ' HS ', ' S CODE ', ' M ', 'COMMODITY', 'CODE NO', 'NALADI SH', ]

code_list2 = ['NCM CODE', 'NCM Code', 'NCM NR', 'NCM Nr', 'NCM HS CODE', 'NCM NUMBER', 'NCM NO', 'NCM HTS', 'NCMs',
              'NCMS', 'NCM HS', 'NCM', ]

pkg_info_dict = {'BALES': 'BLS', 'BARRELS': 'BRL', 'BARS': 'BAR', 'BEAMS': 'BMS', 'BIDONS': 'BDN', 'BIG': 'BGC',
                 'BINS': 'BIN', 'BLOCKS': 'BLK', 'BOBBINS': 'BOB', 'BOTTELON': 'BOT', 'BOTTLES': 'BTL', 'BOXES': 'BXS', 'BUCKETS': 'BKT',
                 'BULK': 'BULK', 'BULTOS': 'BUL', 'BUNDLE': 'BDL', 'CAGE': 'CAG', 'CANS': 'CAN', 'CARTONS': 'CTN', 'CASES': 'CS',
                 'CASKS': 'CSK', 'COILS': 'CLS', 'COLLI': 'COL', 'CRADLES': 'CDL', 'CRATES': 'CRT', 'CYLINDERS': 'CYL', 'DRUMS': 'DRS',
                 'FLASKS': 'FLK', 'FLATS': 'FLT', 'FLEXIBAGS': 'FLB', 'GALLONS': 'GAL', 'GAYLDS': 'GAY', 'IBCS': 'IBC', 'JARS': 'JAR',
                 'JUGS': 'JUG', 'KEGS': 'KGS', 'LIFT VAN': 'LVN', 'LIFTS': 'LFT', 'LITERS': 'LTR', 'LOTS': 'LOT', 'PACKS': 'PKG',
                 'PAILS': 'PLS', 'PAIRS': 'PRS', 'PALLETS': 'PLT', 'PARCELS': 'PCL', 'PIECES': 'PCS', 'PLAST': 'PLB',
                 'PACKAGES': 'PKG', 'PKG': 'PKG', 'PILES': 'PLS', 'PLATES': 'PLA', 'POLIBAGS': 'PBG', 'RACKS': 'RCK', 'REELS': 'RLS',
                 'ROLLS': 'RLL', 'SACKS': 'SAC', 'PALLET': 'PLT', 'CONTAINER': 'CNT', 'SETS': 'SET', 'SHEETS': 'SHT', 'SKIDS': 'SKD',
                 'SLABS': 'SLB', 'SLING BAG': 'SBG', 'SPOOL': 'SPL', 'TANK': 'THK', 'TIERCES': 'TRC', 'TINS': 'TIN', 'TOTE': 'TOT',
                 'TRAYS': 'TRA', 'TUBES': 'TBS', 'UNITS': 'UNT', 'UNKNOWN': 'UNK', 'VAN': 'VAN', 'VOLUMES': 'VOL', 'WOODEN BIN': 'WBI',
                 'BAGS': 'BGS', 'BALE': 'BLS', 'BARREL': 'BRL', 'BAR': 'BAR', 'BEAM': 'BMS', 'BIDON': 'BDN', 'BIG BAG': 'BBG',
                 'BIG CARTON': 'BGC', 'BIN': 'BIN', 'BLOCK': 'BLK', 'BOBBIN': 'BOB', 'BOTTLE': 'BTL', 'BOX': 'BXS', 'BUCKET': 'BKT',
                 'CAN': 'CAN', 'CARTON': 'CTN', 'CASE': 'CS', 'CASK': 'CSK', 'COIL': 'CLS', 'CRADLE': 'CDL', 'CRATE': 'CRT',
                 'CYLINDER': 'CYL', 'DRUM': 'DRS', 'FLASK': 'FLK', 'FLAT': 'FLT', 'FLEXIBAG': 'FLB', 'GALLON': 'GAL', 'GAYLD': 'GAY',
                 'IBC': 'IBC', 'JAR': 'JAR', 'JUG': 'JUG', 'KEG': 'KGS', 'LIFT': 'LFT', 'LITER': 'LTR', 'LOT': 'LOT',
                 'PACK': 'PKG', 'PAIL': 'PLS', 'PAIR': 'PRS', 'PARCEL': 'PCL', 'PIECE': 'PCS', 'PILE': 'PLS', 'PLATE': 'PLA',
                 'POLIBAG': 'PBG', 'RACK': 'RCK', 'REEL': 'RLS', 'ROLL': 'RLL', 'SACK': 'SAC', 'SET': 'SET', 'SHEET': 'SHT',
                 'SKID': 'SKD', 'SLAB': 'SLB', 'TIERCE': 'TRC', 'TIN': 'TIN', 'TRAY': 'TRA', 'TUBE': 'TBS', 'UNIT': 'UNT',
                 'VOLUME': 'VOL', 'BAG': 'BGS', 'PACKAGE': 'PKG'}


def desc(string):
    split_code = " "
    seq_list = []
    weight_seq_list, pkg_seq_list = [], []
    ncm_simple, ncm_var, ncm_sub = 0, 0, 0
    new_string = string.replace('STATUS', '').replace('Status', ' ').replace('AND', '').replace('.', '').replace(',', ' ')\
        .replace('WOODEN', '').replace('   ', ' ').replace('  ', ' ')
    try:
        for code in code_list:
            if str(new_string).__contains__(code):
                split_code = code
                seq_list = new_string.replace('0000 ', ' ').replace('000 ', ' ').replace('00 ', ' ').split(code)[1:]
                pkg_seq_list = new_string.split(code)[:-1]
                weight_seq_list = new_string.split('KGS')[:-1]
                break
        final_ncm_list = capturing_ncm_list(seq_list)
        if split_code == ' ':
            new_string.append(new_string)
            pkg_seq_list.append(new_string)
            if new_string.__contains__(' KG'):
                weight_seq_list.append(new_string)
        else:
            list_sequences = new_string.split(split_code)[:-1]
            for lis in list_sequences:
                if lis.__contains__(' KG'):
                    weight_seq_list.append(lis)
                pkg_seq_list.append(lis)
            if not pkg_seq_list:
                pkg_seq_list.append(new_string)
            if not final_ncm_list:
                if new_string.__contains__('COMMODITY'):
                    seq_list = new_string.split('COMMODITY')[1:]
                elif new_string.__contains__('NCM'):
                    seq_list = new_string.split('NCM')[1:]
                final_ncm_list = capturing_ncm_list(seq_list)
        coded_ncm_list = []
        for new_ncm in final_ncm_list:
            if new_ncm.__contains__(' '):
                new_ncm_list = str(new_ncm).split(' ')
                new_ncm = ''
                for ncm in new_ncm_list:
                    if len(ncm) > 8:
                        ncm = ncm[:8]
                    while len(ncm) < 8:
                        ncm = ncm + '0'
                    new_ncm += ncm + ' '
                coded_ncm_list.append(new_ncm.strip(' '))
            else:
                if len(new_ncm) > 8:
                    new_ncm = new_ncm[:8]
                while len(new_ncm) < 8:
                    new_ncm = new_ncm + '0'
                coded_ncm_list.append(new_ncm.strip(' '))
        ncm_list = coded_ncm_list
        ncm_list = hs8conversion(ncm_list)
        if len(ncm_list) == 0:
            return 6, " ", [], []
        counter = 0
        if len(ncm_list) == 1:
            if str(ncm_list).__contains__(' '):
                new_ncm_list = str(ncm_list).strip("['").strip("']").split(' ')
                for i in range(0, len(new_ncm_list) - 1):
                    if new_ncm_list[i] == new_ncm_list[i + 1]:
                        counter += 1
                if counter == len(new_ncm_list) - 1:
                    return 1, split_code, ncm_list, pkg_seq_list
                return 2, split_code, ncm_list, pkg_seq_list
            return 1, split_code, ncm_list, pkg_seq_list

        if len(ncm_list) > 1:
            i = 0
            while len(ncm_list) - 1:  # to check directly for simple
                if ncm_list[i] in ncm_list[i + 1] or ncm_list[i + 1] in ncm_list[i] \
                        or ncm_list[i][:6] in ncm_list[i + 1][:6] or ncm_list[i + 1][:6] in ncm_list[i][:6]:
                    i += 1
                else:
                    break
                if i == len(ncm_list) - 1:
                    return 1, split_code, ncm_list, pkg_seq_list

        loop = 0
        while loop < 1:
            i = 0
            while i < len(ncm_list) - 1:
                var, sub = 0, 0
                ncmlisti = ncm_list[i].split(" ")
                ncmlistj = ncm_list[i + 1].split(" ")
                if len(ncmlisti) == len(ncmlistj):
                    if len(ncmlisti) == 1 and len(ncmlistj) == 1:
                        if ncm_list[i] in ncm_list[i + 1] or ncm_list[i + 1] in ncm_list[i]:
                            i = i + 1
                            ncm_simple += 1
                        else:
                            i = i + 1
                            sub += 1
                    elif len(ncmlisti) > 1 and len(ncmlistj) > 1:
                        var, sub = diff_all(ncmlisti, ncmlistj)  ########   Differentiate various and sub
                        i = i + 1
                elif not len(ncmlisti) == len(ncmlistj):
                    var, sub = diff_all(ncm_list[i], ncm_list[i + 1])  ########   Differentiate various and sub
                    i = i + 1
                ncm_var += var
                ncm_sub += sub
            loop += 1
        if ncm_sub > 0 and ncm_var > 0:
            return 3, split_code, ncm_list, pkg_seq_list  # various and sub
        elif ncm_simple > 0 and ncm_var > 0:
            return 4, split_code, ncm_list, pkg_seq_list  # simple and various
        elif ncm_simple > 0 and ncm_sub > 0:
            weight_list = fetching_weight_cbm(weight_seq_list)
            if not len(weight_list) == len(ncm_list):
                return 2, split_code, ncm_list, pkg_seq_list  # various
            return 5, split_code, ncm_list, pkg_seq_list  # sub and simple
        elif ncm_simple == len(ncm_list) - 1 and ncm_var == 0 and ncm_sub == 0:
            return 1, split_code, ncm_list, pkg_seq_list  # simple
        elif ncm_var == len(ncm_list) - 1 and ncm_simple == 0 and ncm_sub == 0:
            return 2, split_code, ncm_list, pkg_seq_list  # various
        elif ncm_sub == len(ncm_list) - 1 and ncm_var == 0 and ncm_simple == 0:
            return 3, split_code, ncm_list, pkg_seq_list  # sub
    except:
        return 6, " ", [], []


def fetching_more_ncm(code, list_sequences):
    seq_list = []
    chunkify = list_sequences.split(code)[1:]
    j = 0
    while j < len(chunkify):
        for code in code_list2:
            if str(chunkify[j]).__contains__(code):
                code_seq = str(chunkify[j]).split(code)
                if code_seq:
                    for code_seq1 in code_seq:
                        new_code_seq = code_seq1.replace('0000 ', ' ').replace('000 ', ' ').replace('00 ', ' ').replace('  ', ' ')
                        if new_code_seq != '':
                            seq_list.append(new_code_seq.strip(' '))
                    break
        j += 1
    if not seq_list:
        j = 0
        while j < len(chunkify):
            code_seq = chunkify[j]
            if code_seq != '':
                new_code_seq = code_seq.replace('0000 ', ' ').replace('000 ', ' ').replace('00 ', ' ').replace('  ', ' ')
                seq_list.append(new_code_seq.strip(' '))
            j += 1
    ncm_list = capturing_ncm_list(seq_list)
    new_ncm_list = ' '.join(ncm for ncm in ncm_list)
    return new_ncm_list


def capturing_ncm_list(seq_list):
    ncm_list = []
    for seq1 in seq_list:
        if seq1 == '':
            continue
        ncm_token = '0000'
        if isinstance(seq1, str):
            if len(seq_list) == 1:
                match = re.search(r'([A-Za-z@]+)', seq1.strip(' '))
                if match:
                    ncm_token = seq1.split(match.group())[0].strip(' ')
                    if ncm_token != " ":
                        ncm_token = re.sub(r'( \d{1,3}$)', '', ncm_token)
                        ncm_list.append(ncm_token.strip(" "))
                else:
                    ncm_token = re.search('[0-9 ]+', seq1.strip(" ")).group()
                    ncm_list.append(ncm_token)
                    break
            else:
                match = re.search(r'([A-Za-z@]+)', seq1.strip(' '))
                if match:
                    ncm_token = seq1.split(match.group())[0].strip(' ')
                    if ncm_token != " ":
                        ncm_token = re.sub(r'( \d{1,3}$)', '', ncm_token)
                        ncm_list.append(ncm_token.strip(' '))
                else:
                    ncm_token = re.search('[0-9 ]+', seq1.strip(" ")).group()
                    ncm_list.append(ncm_token)
        else:
            token_list = []
            for code_seq in seq1:
                if code_seq != '':
                    match = re.search(r'([A-Za-z@]+)', code_seq.strip(' '))
                    if match:
                        ncm_token = code_seq.split(match.group())[0].strip(' ')
                        if ncm_token != " ":
                            ncm_token = re.sub(r'( \d{1,3}$)', '', ncm_token)
                    else:
                        ncm_token = re.search('[0-9 ]+', code_seq.strip(" ")).group()
                if ncm_token not in token_list:
                    token_list.append(ncm_token)
                    ncm_token += ''.join(ncm_token)
            ncm_list.append(ncm_token.strip(' '))
    return ncm_list


def diff_all(ncm_i, ncm_j):
    count = 0
    ncm_var, ncm_sub = 0, 0
    try:
        new_i = ncm_i.split(" ")
        new_j = ncm_j.split(" ")
    except:
        new_i = ncm_i
        new_j = ncm_j
    try:
        for na, nb in zip(new_i, new_j):
            if nb in ncm_i or na in ncm_j:
                count += 1
        if count > 0:
            ncm_var += 1
        else:
            ncm_sub += 1
    except:
        return 0, 0, 0
    return ncm_var, ncm_sub


def hs8conversion(uncoded_ncm_list):
    coded_ncm_list = []
    for new_ncm in uncoded_ncm_list:
        if new_ncm.__contains__(' '):
            new_ncm_list = str(new_ncm).split(' ')
            new_ncm = ''
            for ncm in new_ncm_list:
                if len(ncm) == 1 or len(ncm) == 2:
                    continue
                elif len(ncm) > 8:
                    ncm = ncm[:8]
                while len(ncm) < 8:
                    ncm = ncm + '0'
                if ncm == '00000000':
                    continue
                new_ncm += ncm + ' '
            coded_ncm_list.append(new_ncm.strip(' '))
        else:
            if len(new_ncm) > 8:
                new_ncm = new_ncm[:8]
            while len(new_ncm) < 8:
                new_ncm = new_ncm + '0'
            coded_ncm_list.append(new_ncm.strip(' '))
    return coded_ncm_list


def fetching_pkg_qnty(pkg_seq_list):
    pkg_info_list, qnty_info_list = [], []
    try:
        for pkg_seq1 in pkg_seq_list:
            pkg_info, qnty_info = '', ''
            seq = pkg_seq1.strip(' ').split(' ')
            for s1 in seq:
                for pkg_key, pkg_value in pkg_info_dict.items():
                    if pkg_key.upper() in s1.upper():
                        pkg_info = pkg_value
                        pkg_info = pkg_value
                        pattern = "(.*?)(?=" + s1 + ")"
                        qnty = re.search(pattern, pkg_seq1)
                        qnty_info = qnty.group().strip(' ').split(" ")[0]
                if pkg_info != '':
                    break
            pkg_info_list.append(pkg_info)
            qnty_info_list.append(qnty_info)
        # #logger.log.debug("pkg_qnty: pkg and qnty info obtained")
    except Exception as ex:
        logger.log.error("pkg_qnty: While fetching pkg or qnty info", ex)
    return pkg_info_list, qnty_info_list


def fetching_weight_cbm(string):
    weight_list, cbm_list = [], []
    weight, new_cbm = 0.00, 0.00
    for string1 in string:
        string1 = string1.replace(',', '')
        try:
            if ' KGS ' in string1:
                pattern1 = "(\d*\.\d*)(?= KGS )"
            else:
                pattern1 = "(\d*\.\d*)(?= KG )"
            match1 = re.search(pattern1, string1)
            if match1:
                weight = (match1.group()).split(" KG")[0].split(" ")[-1]
        except:
            weight = 0.00
        weight_list.append(weight)
        try:
            pattern2 = "(.*)(?= CBM)"
            match2 = re.search(pattern2, string1)
            if match2:
                cbm = (match2.group()).split("CBM")[0]
                new_cbm = cbm.split(" ")[-1]
        except:
            new_cbm = 0.00
        cbm_list.append(new_cbm)
    return weight_list, cbm_list


def onex_search(input_db, input_file):
    input = xlsxwriter.Workbook(input_file)
    sheet = input.add_worksheet()

    row1 = 0
    sheet.write(row1, 0, "BL")
    sheet.write(row1, 1, "NCM")
    sheet.write(row1, 2, "WEIGHT")
    sheet.write(row1, 3, "CBM")
    sheet.write(row1, 4, "PACKAGE")
    sheet.write(row1, 5, "QUANTITY")
    row1 += 1

    count = 0
    logger.log.debug("Total length of DB = " + str(len(input_db)) + " ")
    for line in input_db:
        try:
            count += 1
            bl_string = str(line[0])
            #logger.log.debug(str(count) + " Started " + bl_string + " ")
            if bl_string == '':
                continue

            string_value = str(line[1]).upper()
            special_char_list1 = ['+', '/', '\\r\\n', '\r\n\r\n', '\r\n', "'", '"', '*', '(', ')', ';', ':', '#',
                                 '*', '&', '?', '@', '>', '<', "'S", 'CONTAIN', '   ', '  ']
            for k in special_char_list1:
                string_value = string_value.replace(k, ' ')

            special_char_list2 = ['STATUS', ' TATUS ', ' ATUS ', ' TUS ', ' US ', '-', '�', '+', '|', '=', '%']
            for k in special_char_list2:
                string_value = string_value.replace(k, '')
            # capturing ncm list and type of BL
            string_value = string_value.replace('NCM CODE', 'NCM').replace('NCM NO', 'NCM').replace('NCM NUMBER', 'NCM')
            try:
                return_value, code, ncm_list, pkg_seq_list = desc(string_value)
            except TypeError as ex:
                logger.log.error("onex: Type Error in NCM capture", ex)
                continue
            ncm_list = hs8conversion(ncm_list)

            if return_value == 1:
                sheet.write(row1, 0, bl_string)
                if len(ncm_list) > 1:
                    try:
                        i = 1
                        ncm_list1 = [ncm_list[0]]
                        while i <= len(ncm_list):
                            if ncm_list[i] not in ncm_list[0] and ncm_list[0] not in ncm_list[i]:
                                ncm_list1.append(ncm_list[i])
                                sheet.write(row1, 1, str(ncm_list1))
                                row1 += 1
                            else:
                                sheet.write(row1, 1, str(ncm_list1))
                                row1 += 1
                            i += 1
                            break
                    except:
                        sheet.write(row1, 1, str(ncm_list))
                        row1 += 1
                else:
                    sheet.write(row1, 1, str(ncm_list))
                    row1 += 1
                #logger.log.debug("onex: Simple NCM captured")

            elif return_value == 2 or return_value == 4:
                sheet.write(row1, 0, bl_string)
                sheet.write(row1, 1, str(ncm_list))
                row1 += 1
                #logger.log.debug("onex: Various NCM captured")

            elif return_value == 3:
                pkg_list, qnty_list = fetching_pkg_qnty(pkg_seq_list)
                string1 = string_value.split(code)[:-1]
                weight_list, cbm_list = fetching_weight_cbm(string1)
                counter = 0
                for i in range(len(weight_list)):
                    i, flag = 0, 0
                    while i < len(weight_list) - 1:
                        if weight_list[i] == weight_list[i + 1]:
                            flag = 1
                        else:
                            flag = 0
                        i += 1
                    if flag == 1:
                        counter += 1
                if counter == len(weight_list):
                    sheet.write(row1, 0, bl_string)
                    sheet.write(row1, 1, str(ncm_list))
                    row1 += 1
                    #logger.log.debug("onex: Various NCM captured")
                    continue
                for ncm, weight, cbm, pkg, qnty in zip(ncm_list, weight_list, cbm_list, pkg_list, qnty_list):
                    sheet.write(row1, 0, bl_string)
                    sheet.write(row1, 1, str(ncm))
                    sheet.write(row1, 2, str(weight))
                    sheet.write(row1, 3, str(cbm))
                    sheet.write(row1, 4, str(pkg))
                    sheet.write(row1, 5, str(qnty))
                    row1 += 1
                #logger.log.debug("onex: Sub Item captured")

            elif return_value == 5:
                pkg_list, qnty_list = fetching_pkg_qnty(pkg_seq_list)
                string1 = string_value.split(code)[:-1]
                weight_list, cbm_list = fetching_weight_cbm(string1)
                for ncm, weight, cbm, pkg, qnty in zip(ncm_list, weight_list, cbm_list, pkg_list, qnty_list):
                    sheet.write(row1, 0, bl_string)
                    sheet.write(row1, 1, str(ncm))
                    sheet.write(row1, 2, str(weight))
                    sheet.write(row1, 3, str(cbm))
                    sheet.write(row1, 4, str(pkg))
                    sheet.write(row1, 5, str(qnty))
                    row1 += 1
                #logger.log.debug("onex: Simple and Sub Item")

            elif return_value == 6:
                try:
                    code = 'COMMODITY'
                    seq1 = string_value.split(code)[1]
                    match = re.search(r'([A-Za-z@]+)', seq1.strip(' '))
                    ncm_token = seq1.split(match.group())[0].strip(' ')
                    if ncm_token != " ":
                        ncm_list.append(ncm_token.strip(' ')[:8])
                    ncm_list = hs8conversion(ncm_list)
                except:
                    ncm_list = '00000000'
                sheet.write(row1, 0, bl_string)
                sheet.write(row1, 1, str(ncm_list))
                row1 += 1
                #logger.log.debug("onex: Unknown Type")

        except Exception as ex:
            logger.log.error("onex: While fetching or updating sheet", ex)

    input.close()
