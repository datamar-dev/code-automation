import os
import re
import sys
import time
import logger
import xlsxwriter


code_list = ['POSICION ARANCELARIA NCM', 'POSICION ARANCELARIA', 'NCM NALADI HS CODE', 'NCM HS CODE NO', 'NCMCODE HS CODE', 'NCM C ODE', 'NCMCODE', 'NCM CODE NO', 'NCM N O', 'NCM CODES', 'NCM CODE IS', 'NCM CODE', 'NCM NR', 'NCM HS CODE',
             'NCM NUMBERS', 'NCM NUMBER IS', 'NCM NUMBER', 'NCM NOS', 'NCM-CODES', 'NCMNO', 'NCM NO', 'NCM HTS', 'NCMS CODE', 'NCMS', 'NCM SH', 'NCM HS', 'NCM HARMONIZED CODE', 'NCM NCM',
             'NCM-NR', 'NCM', 'N CM NO', 'NC M', 'HS CODE NOS', 'HS CODE NO', 'HTS CODE', 'HT S CODE', 'HSN CODE',
             'HS CODE NUMBER', 'NALADI SH', 'NALADI', 'HS NO', 'HSCODENO', 'HS CODE N', 'HS CODES', 'HS CODENUMBER', 'HSCODE NUMBER', 'HS-CODE',
             'HSCODE NO', 'HSCODE', 'HS-CODE', 'HS CODE HS CODE', 'HTS CODE', 'HS NUMBER', 'SPARE PARTS', 'HARMONIZED CODE']


pkg_info_dict_1 = {'BE': 'BDL', 'BD': 'BDL', 'BG': 'BGS', 'FX': 'PKG', 'PK': 'PKG', 'JT': 'PKG', 'LG': 'PKG', 'NE': 'PKG',
    'NG': 'PKG', 'PE': 'PKG', 'RO': 'PKG', 'TY': 'PKG', 'VR': 'PKG', 'VY': 'PKG', 'WA': 'PKG', 'BT': 'BTL', 'CN': 'CNT',
    'BX': 'BXS', 'CR': 'CRT', 'DR': 'DRS', 'CT': 'CTN', 'BA': 'BAR', 'BB': 'BBG', 'CY': 'CYL', 'LV': 'LVN', 'BR': 'BRL',
    'SA': 'SAC', 'CL': 'CLS', 'CO': 'COL', 'CD': 'CDL', 'IB': 'IBC', 'JA': 'JAR', 'JU': 'JUG', 'KG': 'KGS', 'PL': 'PLS',
    'VO': 'VOL', 'LF': 'LFT', 'LT': 'LTR', 'LO': 'LOT', 'RACKS': 'RCK', 'BI': 'BIN', 'BO': 'BOT', 'BM': 'BMS', 'UN': 'UNT',
    'SE': 'SET', 'SH': 'SHT', 'SK': 'SKD', 'SL': 'SLB', 'SB': 'SBG', 'SP': 'SPL', 'TK': 'THK', 'TH': 'THK', 'TI': 'TIN',
    'TO': 'TOR', 'TB': 'TBS', 'VA': 'VAN', 'WB': 'WBI', 'PC': 'PCS', 'PR': 'PRS', 'PB': 'PBG', 'BK': 'BKT',  'BU': 'BUL'}

pkg_info_dict_2 = {'BALES': 'BLS', 'BARRELS': 'BRL', 'BARS': 'BAR', 'BEAMS': 'BMS', 'BIDONS': 'BDN', 'BIG': 'BGC', 'BUNDLES': 'BDL',
                 'BINS': 'BIN', 'BLOCKS': 'BLK', 'BOBBINS': 'BOB', 'BOTTELON': 'BOT', 'BOTTLES': 'BTL', 'BOXES': 'BXS',
                 'BUCKETS': 'BKT', 'BULK': 'BKC', 'BULTOS': 'BUL', 'BUNDLE': 'BDL', 'CAGE': 'CAG', 'CANS': 'CAN', 'CARTONS': 'CTN',
                 'CASES': 'CS',  'CASKS': 'CSK', 'COILS': 'CLS', 'COLLI': 'COL', 'CRADLES': 'CDL', 'CRATES': 'CRT', 'CYLINDERS': 'CYL',
                 'DRUMS': 'DRS', 'FLASKS': 'FLK', 'FLATS': 'FLT', 'FLEXIBAGS': 'FLB', 'GALLONS': 'GAL', 'GAYLDS': 'GAY', 'IBCS': 'IBC',
                 'JARS': 'JAR', 'JUGS': 'JUG', 'KEGS': 'KGS', 'LIFT VAN': 'LVN', 'LIFTS': 'LFT', 'LITERS': 'LTR', 'LOTS': 'LOT',
                 'PACKS': 'PKG', 'PAILS': 'PLS', 'PAIRS': 'PRS', 'PALLETS': 'PLT', 'PARCELS': 'PCL', 'PIECES': 'PCS',
                 'PLAST./BAG': 'PLB', 'PACKAGES': 'PKG', 'PKG': 'PKG', 'PILES': 'PLS', 'PLATES': 'PLA', 'POLIBAGS': 'PBG', 'RACKS': 'RCK',
                 'REELS': 'RLS', 'ROLLS': 'RLL', 'SACKS': 'SAC', 'PALLET': 'PLT', 'CONTAINER': 'CNT',
                 'SETS': 'SET', 'SHEETS': 'SHT', 'SKIDS': 'SKD', 'SLABS': 'SLB', 'SLING BAG': 'SBG', 'SPOOL': 'SPL',
                 'TANK': 'THK', 'TIERCES': 'TRC', 'TINS': 'TIN', 'TOTE': 'TOT', 'TRAYS': 'TRA', 'TUBES': 'TBS', 'UNITS': 'UNT',
                 'UNKNOWN': 'UNK', 'VAN': 'VAN', 'VOLUMES': 'VOL', 'WOODEN BIN': 'WBI', 'BAGS': 'BGS', 'BALE': 'BLS', 'BARREL': 'BRL',
                 'BAR': 'BAR', 'BEAM': 'BMS', 'BIDON': 'BDN', 'BIG BAG': 'BBG', 'BIN': 'BIN', 'BLOCK': 'BLK', 'BOBBIN': 'BOB',
                 'BOTTLE': 'BTL', 'BOX': 'BXS', 'BUCKET': 'BKT', 'CAN': 'CAN', 'CARTON': 'CTN', 'CASE': 'CS', 'CAS': 'CS', 'CASK': 'CSK',
                 'COIL': 'CLS', 'CRADLE': 'CDL', 'CRATE': 'CRT', 'CYLINDER': 'CYL', 'DRUM': 'DRS', 'FLASK': 'FLK', 'FLAT': 'FLT',
                 'FLEXIBAG': 'FLB', 'GALLON': 'GAL', 'GAYLD': 'GAY', 'IBC': 'IBC', 'JAR': 'JAR', 'JUG': 'JUG', 'KEG': 'KGS',
                 'LIFT': 'LFT', 'LITER': 'LTR', 'LOT': 'LOT', 'PACKAGE': 'PKG', 'PACK': 'PKG', 'PAIL': 'PLS', 'PAIR': 'PRS',
                 'PARCEL': 'PCL', 'PIECE': 'PCS', 'PILE': 'PLS', 'PLATE': 'PLA', 'POLIBAG': 'PBG', 'RACK': 'RCK', 'REEL': 'RLS',
                 'ROLL': 'RLL', 'SACK': 'SAC', 'SET': 'SET', 'SHEET': 'SHT', 'SKID': 'SKD', 'SLAB': 'SLB', 'TIERCE': 'TRC', 'TIN': 'TIN',
                 'TRAY': 'TRA', 'TUBE': 'TBS', 'UNIT': 'UNT', 'VOLUME': 'VOL', 'BAG': 'BGS', }

sequence_split = '------------------------------------------------------------'
split_pattern = '- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -'
string_value = ' '


def desc(string):
    split_code = " "
    seq_list, final_ncm_list = [], []
    ncm_simple, ncm_var, ncm_sub = 0, 0, 0
    new_string = string.replace('AND', '').replace(',', ' ').replace(' - ', ' ').replace('   ', ' ').replace('  ', ' ').replace(' .', '').replace('.', '')
    new_string = new_string.replace('HS CODES', 'HS CODE').replace('NCM NUMBE R', 'NCM NUMBER')
    pattern1 = "(?<=VOLUME )(\d* \w*)(?= )"
    new_string = (re.sub(pattern1, '', new_string.strip(' '))).replace(' VOLUME  ', '')
    for eachkey, eachvalue in pkg_info_dict_2.items():
        pattern1 = "(\d* )(?="+eachkey+")"
        new_string = re.sub(pattern1, '', new_string.strip(' '))
    try:
        for code in code_list:
            if str(new_string).__contains__(code):
                split_code = code
                seq_list = new_string.split(code)[1:]
                break
        final_ncm_list = capturing_ncm_list(seq_list)
        if sequence_split in string and split_pattern in string:
            weight_seq = string.split(sequence_split)[-1].strip(' ')
            pkg_seq_list = weight_seq.split(split_pattern)[:-1]
        else:
            weight_seq = string.split('GROSS')
            pkg_seq_list = weight_seq[:-1]
        if not final_ncm_list:
            if 'HARMONIZED CODE' in new_string:
                split_code = 'HARMONIZED CODE'
            else:
                split_code = 'COMMODITY'
            seq_list = new_string.replace(' 0000 ', '').replace(' 000 ', '').replace(' 00 ', '').replace('  ', ' ').split(split_code)[1:]
            final_ncm_list = capturing_ncm_list(seq_list)
        if not final_ncm_list:
            for ncm_str in pkg_seq_list:
                new_ncm = ncm_str.strip(' ').split(' ')[-2]
                new_ncm = re.sub(r'[A-Za-z]+', '', new_ncm)
                final_ncm_list.append(new_ncm)
        ncm_list = final_ncm_list
        ncm_list = hs8conversion(ncm_list)
        if len(checking_sub(pkg_seq_list)) == 1:
            new_list = []
            ncm_str = ''
            for ncm in ncm_list:
                ncm_str += ''.join(ncm) + ' '
            new_list.append(ncm_str.strip(' '))
            ncm_list = new_list
        if len(ncm_list) == 0:
            return 6, split_code, [], []
        counter = 0
        updated_ncm_list = []
        if len(ncm_list) == 1:
            if str(ncm_list).__contains__(' '):
                new_ncm_list = str(ncm_list).strip("['").strip("']").split(' ')
                for i in range(0, len(new_ncm_list) - 1):
                    if new_ncm_list[i] == new_ncm_list[i+1] or new_ncm_list[i] in new_ncm_list[i+1] or new_ncm_list[i+1] in new_ncm_list[i]:
                        counter += 1
                if counter == len(new_ncm_list) - 1:
                    updated_ncm_list.append(new_ncm_list[0])
                    ncm_list = updated_ncm_list
                    return 1, split_code, ncm_list, pkg_seq_list
                return 2, split_code, ncm_list, pkg_seq_list
            return 1, split_code, ncm_list, pkg_seq_list
        updated_ncm_list = []
        if len(ncm_list) > 1:
            i, count = 0, 0
            if len(ncm_list) == 2:
                if ncm_list[i].__contains__(' '):
                    new_ncm1 = ncm_list[i].split(' ')
                    if len(new_ncm1) > 1:
                        for j in range(0, len(new_ncm1) - 1):
                            if new_ncm1[j] == new_ncm1[j + 1] or new_ncm1[j][:6] in new_ncm1[j + 1][:6]:
                                updated_ncm_list.append(ncm_list[j][:6])
                            else:
                                break
                updated_ncm_list = []
                if ncm_list[i] in ncm_list[i + 1] or ncm_list[i + 1] in ncm_list[i]:
                    updated_ncm_list.append(ncm_list[i])
                    ncm_list = updated_ncm_list
                    return 1, split_code, ncm_list, pkg_seq_list
                elif ncm_list[i][:6] in ncm_list[i + 1]:
                    updated_ncm_list.append(ncm_list[i + 1])
                    ncm_list = updated_ncm_list
                    return 1, split_code, ncm_list, pkg_seq_list
                elif ncm_list[i + 1][:6] in ncm_list[i]:
                    updated_ncm_list.append(ncm_list[i])
                    ncm_list = updated_ncm_list
                    return 1, split_code, ncm_list, pkg_seq_list
            while i < len(ncm_list) - 1:  # to check directly for simple
                if len(ncm_list[i].split(' ')) == len(ncm_list[i+1].split(' ')):
                    if ncm_list[i] in ncm_list[i+1] or ncm_list[i+1] in ncm_list[i]:
                        count += 1
                        i += 1
                        continue
                    elif ' ' not in ncm_list[i] and ' ' not in ncm_list[i + 1]:
                        if ncm_list[i][:6] in ncm_list[i+1][:6] or ncm_list[i+1][:6] in ncm_list[i][:6]:
                            if not str(updated_ncm_list).__contains__(ncm_list[i][:6]):
                                new_ncm1 = ncm_list[i].split(' ')
                                if len(new_ncm1) > 1:
                                    for i in range(0, len(new_ncm1) - 1):
                                        if new_ncm1[i] == new_ncm1[i+1] or new_ncm1[i][:6] in new_ncm1[i+1][:6]:
                                            updated_ncm_list.append(ncm_list[i][:6])
                                        else:
                                            count += 1
                                            i += 1
                                            break
                            else:
                                updated_ncm_list.append(ncm_list[i])
                        else:
                            break
                        count += 1
                        i += 1
                        continue
                    else:
                        break
                elif ' ' not in ncm_list[i] and ' ' not in ncm_list[i+1]:
                    if ncm_list[i][:6] == ncm_list[i + 1][:6]:
                        updated_ncm_list.append(ncm_list[i][:6])
                    count += 1
                    i += 1
                    continue
                else:
                    break
            if count == len(ncm_list)-1:
                if updated_ncm_list:
                    ncm_list = updated_ncm_list
                return 1, split_code, ncm_list, pkg_seq_list
        loop = 0
        while loop < 1:
            i = 0
            while i < len(ncm_list) - 1:
                var, sub = 0, 0
                ncmlisti = ncm_list[i].split(" ")
                ncmlistj = ncm_list[i + 1].split(" ")
                if len(ncmlisti) == len(ncmlistj):
                    if len(ncmlisti) == 1 and len(ncmlistj) == 1:
                        if ncm_list[i] in ncm_list[i + 1] or ncm_list[i + 1] in ncm_list[i]:
                            i = i + 1
                            ncm_simple += 1
                        else:
                            i = i + 1
                            sub += 1
                    elif len(ncmlisti) > 1 and len(ncmlistj) > 1:
                        var, sub = diff_all(ncmlisti, ncmlistj)  ########   Differentiate various and sub
                        i = i + 1
                elif not len(ncmlisti) == len(ncmlistj):
                    var, sub = diff_all(ncm_list[i], ncm_list[i + 1])  ########   Differentiate various and sub
                    i = i + 1
                ncm_var += var
                ncm_sub += sub
            loop += 1
        if ncm_sub > 0 and ncm_var > 0:
            return 3, split_code, ncm_list, pkg_seq_list  # various and sub
        elif ncm_simple > 0 and ncm_var > 0:
            return 4, split_code, ncm_list, pkg_seq_list  # simple and various
        elif ncm_simple > 0 and ncm_sub > 0:
            if not len(fetching_weight(pkg_seq_list)) == len(ncm_list):
                return 2, split_code, ncm_list, pkg_seq_list  # various
            return 5, split_code, ncm_list, pkg_seq_list      # sub and simple
        elif ncm_simple == len(ncm_list) - 1 and ncm_var == 0 and ncm_sub == 0:
            return 1, split_code, ncm_list, pkg_seq_list  # simple
        elif ncm_var == len(ncm_list) - 1 and ncm_simple == 0 and ncm_sub == 0:
            return 2, split_code, ncm_list, pkg_seq_list       # various
        elif ncm_sub == len(ncm_list) - 1 and ncm_var == 0 and ncm_simple == 0:
            return 3, split_code, ncm_list, pkg_seq_list         # sub
    except:
        return 6, " ", [], []


def checking_sub(seq_list):
    new_list = []
    for seq in seq_list:
        try:
            ncm_token = seq.strip(' ').split(' ')[-2].strip(' ')
            if ncm_token not in new_list:
                new_list.append(ncm_token)
        except:
            new_list = []
    return new_list


def hs8conversion(uncoded_ncm_list):
    coded_ncm_list = []
    for new_ncm in uncoded_ncm_list:
        if new_ncm == '':
            continue
        if new_ncm.__contains__(' '):
            new_ncm_list = str(new_ncm).replace('-', '').split(' ')
            new_ncm = ''
            for ncm in new_ncm_list:
                if len(ncm) == 1 or len(ncm) == 2:
                    continue
                elif len(ncm) > 8:
                    ncm = ncm[:8]
                while len(ncm) < 8:
                    ncm = ncm + '0'
                if ncm == '00000000':
                    continue
                new_ncm += ncm + ' '
            coded_ncm_list.append(new_ncm.strip(' '))
        else:
            new_ncm = new_ncm.replace('-', '')
            if len(new_ncm) > 8:
                new_ncm = new_ncm[:8]
            while len(new_ncm) < 8:
                new_ncm = new_ncm + '0'
            coded_ncm_list.append(new_ncm.strip(' '))
    return coded_ncm_list


def capturing_ncm_list(seq_list):
    ncm_list = []
    for seq1 in seq_list:
        if seq1 == '':
            continue
        seq1 = seq1.replace('- ', '').replace('-', '').replace('_', '').replace('.', '').replace(' 0000 ', ' ').replace(' 000 ', ' ').replace(' 00 ', ' ').replace(' 0 ', ' ')
        if seq1.strip(' ').startswith('VOLUME'):
            pattern1 = "(?<=VOLUME )(\d*)(?= CBM)"
            seq1 = re.sub(pattern1, '', seq1.strip(' '))
            seq1 = seq1.replace('VOLUME', '').replace('CBM', '')
        ncm_token = '0000'
        if isinstance(seq1, str):
            if len(seq_list) == 1:
                match = re.search(r'([A-Za-z@]+)', seq1.strip(' '))
                if match:
                    ncm_token = seq1.split(match.group())[0].strip(' ')
                    if ncm_token != " ":
                        ncm_token = re.sub(r'( \d{1,3}$)', '', ncm_token)
                        if ncm_token == '':
                            ncm_list = []
                        else:
                            ncm_list.append(ncm_token.strip(" "))
                else:
                    ncm_token = re.search('[0-9 ]+', seq1.strip(" ")).group()
                    if ncm_token != '':
                        ncm_list.append(ncm_token.strip(" "))
            else:
                match = re.search(r'([A-Za-z@]+)', seq1.strip(' '))
                if match:
                    ncm_token = seq1.split(match.group())[0].strip(' ')
                    if ncm_token != " ":
                        ncm_token = re.sub(r'( \d{1,3}$)', '', ncm_token)
                        if ncm_token != '':
                            ncm_list.append(ncm_token.strip(" "))
                else:
                    ncm_token = re.search('[0-9 ]+', seq1.strip(" ")).group()
                    if ncm_token != '':
                        ncm_list.append(ncm_token.strip(" "))
        else:
            token_list = []
            for code_seq in seq1:
                if code_seq != '':
                    match = re.search(r'([A-Za-z@]+)', code_seq.strip(' '))
                    if match:
                        ncm_token = code_seq.split(match.group())[0].strip(' ')
                        if ncm_token != " ":
                            ncm_token = re.sub(r'( \d{1,3}$)', '', ncm_token)
                    else:
                        ncm_token = re.search('[0-9 ]+', code_seq.strip(" ")).group()
                if ncm_token not in token_list:
                    token_list.append(ncm_token)
                    ncm_token += ''.join(ncm_token)
            ncm_list.append(ncm_token.strip(' '))
    return ncm_list


def diff_all(ncm_i, ncm_j):
    count = 0
    ncm_var, ncm_sub = 0, 0
    try:
        new_i = ncm_i.split(" ")
        new_j = ncm_j.split(" ")
    except:
        new_i = ncm_i
        new_j = ncm_j
    try:
        for na, nb in zip(new_i, new_j):
            if nb in ncm_i or na in ncm_j:
                count += 1
        if count > 0:
            ncm_var += 1
        else:
            ncm_sub += 1
    except:
        return 0, 0, 0
    return ncm_var, ncm_sub


def fetching_pkg_qnty_1(pkg_qnty_list, string):
    qnty_str = ' '
    pkg_list, qnty_list = [], []
    for ncm_str in pkg_qnty_list:
        pkg, qnty = ' ', ' '
        pkg_qnty = ncm_str.strip(' ').replace('-', ' ').replace(',', '').split(' ')[0]
        if re.search(r'(^00\d+[A-Z]+)', pkg_qnty):
            qnty_str = pkg_qnty
        else:
            pkg_qnty = ncm_str.strip(' ').replace('-', ' ').split(' ')
            for seq in pkg_qnty:
                if re.search(r'(^00\d+[A-Z]+)', seq):
                    qnty_str = seq
                    break
        qnty = re.sub(r'[A-Z]+[0-9A-Z]*$', '', qnty_str)
        pkg_value = qnty_str.split(qnty)[1][:2]
        if pkg_value == 'BL':
            if string.__contains__('BLOCKS'):
                pkg = 'BLK'
            else:
                pkg = 'BLS'
        elif pkg_value == 'CA':
            if string.__contains__('CAN'):
                pkg = 'CAN'
            else:
                pkg = 'CAG'
        elif pkg_value == 'RL':
            if string.__contains__('ROLLS'):
                pkg = 'RLL'
            else:
                pkg = 'RLS'
        elif pkg_value == 'TR':
            if string.__contains__('TRAYS'):
                pkg = 'TRA'
            else:
                pkg = 'TRC'
        elif pkg_value == 'GA':
            if string.__contains__('GALLONS'):
                pkg = 'GAL'
            else:
                pkg = 'GAY'
        elif pkg_value == 'FL':
            if string.__contains__('FLASKS'):
                pkg = 'FLK'
            elif string.__contains__('FLEXIBAGS'):
                pkg = 'FLB'
            else:
                pkg = 'FLT'
        elif pkg_value == 'PL':
            if string.__contains__('PALLET'):
                pkg = 'PLT'
            else:
                pkg = 'PLS'
        elif pkg_value in pkg_info_dict_1:
            for key, value in pkg_info_dict_1.items():
                if pkg_value == key:
                    pkg = value
        else:
            pkg = pkg_value
        pkg_list.append(pkg)
        qnty_list.append(qnty)
    return pkg_list, qnty_list


def fetching_pkg_qnty_2(pkg_qnty_list):
    pkg_list, qnty_list = [], []
    for pkg_qnty in pkg_qnty_list:
        pattern2 = "(?<=VOLUME)(.*)(?=CBM)"
        pkg_qnty = (re.sub(pattern2, '', pkg_qnty)).replace(' VOLUMECBM ', ' ')
        pkg, qnty = ' ', ' '
        new_list = str(pkg_qnty).strip(' ').split(' ')
        for each_str in new_list[::-1]:
            for key, value in pkg_info_dict_2.items():
                if key == each_str:
                    pkg = value
                    pattern1 = "(.*)(?=" + each_str + ")"
                    qnty_info = re.search(pattern1, pkg_qnty)
                    qnty = qnty_info.group().strip(' ').split(" ")[-1]
            if pkg != ' ':
                break
        pkg_list.append(pkg)
        qnty_list.append(qnty)
    return pkg_list, qnty_list


def fetching_weight(weight_seq_list):
    # logger.log.debug("Fetching weight List")
    weight_list = []
    for wei_1 in weight_seq_list:
        try:
            flag = 0
            wei_string = ' '
            weight = wei_1.strip(' ').split(' ')[-1][:-3]            # stripping last 3 characters to obtain weight
            for wei in weight:
                if flag == 0 and wei == '0':
                    continue
                else:
                    flag = 1
                    wei_string += wei
            weight_list.append(wei_string.strip(' '))
        except Exception as ex:
            logger.log.error("fetching_weight_list: While fetching weight", ex)
    return weight_list


def fetching_weight_cbm(weight_seq_list):
    weight_list, cbm_list = [], []
    weight, cbm = 0.00, 0.00
    for wei_1 in weight_seq_list:
        wei_1 = wei_1.replace(' .', '.').replace('. ', '.')
        try:
            if 'KGS' in str(wei_1):
                pattern1 = "(?<=GROSS)(.*)(?=KGS)"
            else:
                pattern1 = "(?<=GROSS)(.*)(?=KG)"
            match1 = re.search(pattern1, wei_1)
            if match1:
                weight = (match1.group()).split(" KGS")[0].strip(" ").replace(' ', '')
        except:
            weight = 0.00
        weight_list.append(weight)
        if 'VOLUME' in str(wei_1):
            pattern2 = "(?<=VOLUME)(.*)(?=CBM)"
        else:
            pattern2 = "(?<=CBM)(.*)"
        try:
            match2 = re.search(pattern2, wei_1)
            if match2:
                cbm = (match2.group()).strip(" ").split(' ')[0]
        except:
            cbm = 0.00
        cbm_list.append(cbm)
    return weight_list, cbm_list


def mscu_search(input_db, input_file):
    global string_value
    input = xlsxwriter.Workbook(input_file)
    sheet = input.add_worksheet()
    row1 = 0
    sheet.write(row1, 0, "BL")
    sheet.write(row1, 1, "NCM")
    sheet.write(row1, 2, "WEIGHT")
    sheet.write(row1, 3, "CBM")
    sheet.write(row1, 4, "PACKAGE")
    sheet.write(row1, 5, "QUANTITY")
    row1 += 1

    bl_count = 0
    logger.log.debug("Total length of DB = " + str(len(input_db)) + " ")
    for line in input_db:
        try:
            if 'PLATE' in line[3]:
                string_value = str(line[1]).split('Total no. of ')[0].upper()               # TEXTO field
            else:
                string_value = str(line[2]).upper()                                         # MEMO field
            bl_count += 1
            bl_string = str(line[0])
            #logger.log.debug(str(bl_count) + " Started " + bl_string + " ")
            if bl_string == '':
                continue
            special_char_list1 = ['+', '/', '\\r\\n', '\r\n\r\n', '\r\n', '\t', ''"'", '"', '*', '(', ')', '[', ']',  ';', ':', '#', '&',
                                  '?', ',', '@', '>', '<', "'S", ' S ',  ' X ', '-ST', ' BN ', 'FCL CONTAINER', 'INVOICE ',  'WOODEN', ' W ', '   ', '  ', '  ']
            for k in special_char_list1:
                string_value = string_value.replace(k, ' ')

            special_char_list2 = ['BODY TEXT', 'QUANTITY', 'FREIGHT COLLECT', 'HIGH CUBE', 'SHIPPER DESCRIPTION', 'RATE VIA MONTEVIDEO', 'AS PER SHIPPER .',
                                'TOTAL CBM', 'CARTONSOF', 'SHIPPED', 'NW ', 'FREIGHT', 'PACKED', 'RUC ', 'DECLARED', '�', '+', '|', '=', '%',
                                  'TREATED AND CERTIFIED', 'SERVICE CONTRACT', 'RAIL TRUCK', 'RAIL ', 'NON-HAZ ', 'SPOT QUOTATION', 'TRUCK DOOR',
                                  'GENERAL TARIFF', 'TOTAL', 'H.S.CODE NCM', 'LOT NUMBER', 'ORIGINAL VESSEL VOYAGE', 'HIGH PRIORITY CONTAINER',
                                  'HIGH PRIORITY', 'PRIORITY CONTAINER', ' WEIGHT ']
            for k in special_char_list2:
                string_value = string_value.replace(k, '')
            string_value = string_value.replace('GR WT', 'GROSS')
            # logger.log.debug("mscu: String replacement completed  @ level 1")

            try:
                return_value, code, ncm_list, pkg_seq_list = desc(string_value)
            except TypeError as ex:
                logger.log.error("mscu: Type Error in NCM capture", ex)
                continue

            ncm_list = hs8conversion(ncm_list)
            if return_value == 3 or return_value == 5:
                cbm_count = string_value.count('CBM')
                if cbm_count < len(ncm_list):
                    if cbm_count > len(ncm_list):
                        seq_list = string_value.split('HARMONIZED CODE')[1:]
                        final_ncm_list = capturing_ncm_list(seq_list)
                        ncm_list = hs8conversion(final_ncm_list)
                        return_value = 3
                        code = 'HARMONIZED CODE'
                    else:
                        return_value = 2

            if len(ncm_list) == 0 or ncm_list == ['00000000']:
                ncm_list = fetching_har_code(string_value)
            string1 = []
            new_string = string_value.replace(',', '').replace('GROSS WEIGHT', 'GROSS').replace(' WT', ' ').split(sequence_split)
            string_value = string_value.replace(', ', ' ').replace(',', ' ').replace('  00', '')
            string_value = string_value.replace('PIECESPAINEL', 'PAINEL').replace('MELONDUE', 'MELON').replace('OFWOOD', 'WOOD')\
                .replace('PIECESPINE', 'PINE').replace('DRUMSWOOD', 'DRUMS').replace('WITHFRESH', 'FRESH').replace('CONTAINING', '')\
                .replace('WITHFROZEN', 'FROZEN').replace('CARTONSFROZEN', 'FROZEN').replace('OFFROZEN', 'FROZEN')

            if return_value == 1:
                sheet.write(row1, 0, bl_string)
                ncm_list1 = ncm_list[0].split(' ')
                if len(ncm_list1) > 1:
                    i = 0
                    while i < len(ncm_list1) - 1:
                        if ncm_list1[i] == ncm_list1[i + 1] or ncm_list1[i][:6] == ncm_list1[i + 1][:6]:
                            i += 1
                        else:
                            break
                    if i == len(ncm_list1) - 1:
                        sheet.write(row1, 1, str(ncm_list1[0]))
                        row1 += 1
                    else:
                        sheet.write(row1, 1, str(ncm_list[0]))
                        row1 += 1
                elif len(ncm_list) > 1:
                    try:
                        i = 1
                        ncm_list1 = [ncm_list[0]]
                        while i <= len(ncm_list):
                            if ncm_list[i] not in ncm_list[0] and ncm_list[0] not in ncm_list[i]:
                                ncm_list1.append(ncm_list[i])
                                sheet.write(row1, 1, str(ncm_list1))
                                row1 += 1
                            else:
                                sheet.write(row1, 1, str(ncm_list1))
                                row1 += 1
                            i += 1
                            break
                    except:
                        sheet.write(row1, 1, str(ncm_list))
                        row1 += 1
                else:
                    sheet.write(row1, 1, str(ncm_list))
                    row1 += 1
                #logger.log.debug("mscu: Simple NCM captured")

            elif return_value == 2 or return_value == 4:
                sheet.write(row1, 0, bl_string)
                sheet.write(row1, 1, str(ncm_list))
                row1 += 1
                #logger.log.debug("mscu: Various NCM captured")

            elif return_value == 3:
                ncm_check_list = []
                if sequence_split in string_value and split_pattern in string_value:
                    pkg_list, qnty_list = fetching_pkg_qnty_1(pkg_seq_list, string_value)
                    weight_seq_list = string_value.split(sequence_split)[-1].strip(' ').split(split_pattern)[:-1]
                    weight_list = fetching_weight(weight_seq_list)
                    cbm_list = []
                else:
                    weight_seq_list = []
                    pkg_list, qnty_list = fetching_pkg_qnty_2(pkg_seq_list)
                    seq_list = new_string[0].split('GROSS')[1:]
                    for each_wei in seq_list:
                        each_wei = 'GROSS' + each_wei
                        weight_seq_list.append(each_wei)
                    weight_list, cbm_list = fetching_weight_cbm(weight_seq_list)
                counter = 0
                for i in range(len(weight_seq_list)):
                    try:
                        ncm_token = re.search(r'\w+\d+$', weight_seq_list[i].split('    ')[0]).group(0)
                        token = re.sub('\D+', '', ncm_token)
                    except:
                        ncm_token = re.search(r' \d+ ', weight_seq_list[i]).group(0)
                        token = ncm_token.strip(' ')
                    ncm_check_list.append(token)
                if len(ncm_check_list) > 1:
                    for i in range(0, len(ncm_check_list)):
                        try:
                            if ncm_check_list[i] in ncm_list[i]:
                                counter += 1
                            else:
                                break
                            i += 1
                        except:
                            break
                    if counter == len(ncm_list)-1:
                        sheet.write(row1, 0, bl_string)
                        sheet.write(row1, 1, str(ncm_list))
                        row1 += 1
                        #logger.log.debug("mscu: Various NCM captured")
                        continue
                for ncm, weight, cbm, pkg, qnty in zip(ncm_list, weight_list, cbm_list, pkg_list, qnty_list):
                    sheet.write(row1, 0, bl_string)
                    sheet.write(row1, 1, str(ncm))
                    sheet.write(row1, 2, str(weight))
                    sheet.write(row1, 3, str(cbm))
                    sheet.write(row1, 4, str(pkg))
                    sheet.write(row1, 5, str(qnty))
                    row1 += 1
                #logger.log.debug("mscu: Sub Item captured")

            elif return_value == 5:
                for new in new_string:
                    string1.append(new)
                if sequence_split in string_value and split_pattern in string_value:
                    pkg_list, qnty_list = fetching_pkg_qnty_1(pkg_seq_list, string_value)
                    weight_seq_list = string_value.split(sequence_split)[-1].strip(' ').split(split_pattern)[:-1]
                    weight_list = fetching_weight(weight_seq_list)
                    cbm_list = []
                else:
                    weight_seq_list = []
                    pkg_list, qnty_list = fetching_pkg_qnty_2(pkg_seq_list)
                    seq_list = new_string[0].split('GROSS')[1:]
                    for each_wei in seq_list:
                        each_wei = 'GROSS' + each_wei
                        weight_seq_list.append(each_wei)
                    weight_list, cbm_list = fetching_weight_cbm(weight_seq_list)
                for ncm, weight, cbm, pkg, qnty in zip(ncm_list, weight_list, cbm_list, pkg_list, qnty_list):
                    sheet.write(row1, 0, bl_string)
                    sheet.write(row1, 1, str(ncm))
                    sheet.write(row1, 2, str(weight))
                    sheet.write(row1, 3, str(cbm))
                    sheet.write(row1, 4, str(pkg))
                    sheet.write(row1, 5, str(qnty))
                    row1 += 1
                #logger.log.debug("mscu: Simple and Sub Item")

            elif return_value == 6:
                ncm_list = []
                try:
                    string_value = str(string_value).strip(' ').split('------------------------------------------------------------')[-1]
                    seq1 = string_value.strip(' ').split('- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -')
                    for each_seq in seq1:
                        seq = str(each_seq).strip(' ').split(' ')
                        if 1 < len(seq) < 8:
                            ncm = re.sub('[A-Z]+', '', seq[-2])
                            if ncm not in ncm_list:
                                ncm_list.append(ncm)
                    ncm = hs8conversion(ncm_list)
                except:
                    ncm = '00000000'
                sheet.write(row1, 0, bl_string)
                sheet.write(row1, 1, str(ncm))
                row1 += 1
                #logger.log.debug("mscu: Unknown Type")

        except Exception as ex:
            logger.log.error("mscu: While fetching or updating sheet", ex)

    input.close()


def fetching_har_code(string_value):
    ncm_list = []
    if 'HARMONIZED CODE' in string_value:
        string_value = string_value.strip(' ').replace('   ', ' ').replace('  ', ' ').split('HARMONIZED CODE')[1]
    elif 'FYT' in string_value:
        string_value = string_value.strip(' ').replace('   ', ' ').replace('  ', ' ').split('FYT')[1]
    else:
        return '00000000'
    match = re.search(r'([A-Za-z@]+)', string_value.strip(' '))
    if match:
        ncm_token = string_value.split(match.group())[0].strip(' ')
        ncm_list.append(ncm_token)
        ncm = hs8conversion(ncm_list)
        return ncm
    else:
        return '00000000'


