import logger
from mscu_code_search import *
from DB_Insertion import *


def MSCU_automation(input_db, database_name, input_file, config, conn1, excursor):
    start_time = time()
    try:
        logger.log.debug('main: all modules initialized')
        mscu_search(input_db, input_file)
        logger.log.debug("Extraction Completed")
        output = insertion_main(input_file, excursor, conn1, database_name, config)
        if output == 1:
            print("Output Generated")
        else:
            print("Error while generating output")
        # os.remove(input_file)
        end_time = time()
        hours, rem = divmod(end_time - start_time, 3600)
        minutes, seconds = divmod(rem, 60)
        execution_time = "%d:%d:%d" % (int(hours), int(minutes), seconds)
        print("Execution Time: ", execution_time)
    except Exception as ex:
        logger.log.error("Error occured", exc_info=ex)
