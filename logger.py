from logging.handlers import TimedRotatingFileHandler
import logging
from time import asctime, gmtime, localtime, strftime
import os

log: logging.Logger = None


def init_logging(logConfig):
    global log
    fmt = '[%(asctime)s] [%(thread)-8s] [%(levelname)s] [%(message)s]'
    logFormatter = logging.Formatter(fmt, '%d-%m-%Y %H:%M:%S')
    
    log = logging.getLogger()
    log.setLevel(logConfig['level'])
    
    intvl = logConfig['interval']
    cur_date = strftime("%d-%m-%Y", localtime()) 
    cur_time = strftime("%H-%M-%S", localtime())
    os.makedirs("logs/" + cur_date, exist_ok=True)
    fileHandler = logging.FileHandler(filename="{0}/{1}.log".format("logs/" + cur_date, cur_time), mode='w')
    fileHandler.setFormatter(logFormatter)
    log.addHandler(fileHandler)
    
    if logConfig['console-log'] == True:
        consoleHandler = logging.StreamHandler()
        consoleHandler.setFormatter(logFormatter)
        log.addHandler(consoleHandler)
