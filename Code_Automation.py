import sys
import json
import logger
import pyodbc
import xml.etree.ElementTree as ET
from HSUD_Code import HSUD_automation
from HAPAG_Code import HAPAG_automation
from ONEX_Code import ONEX_automation
from MSCU_Code import MSCU_automation
from MAEU_Code import MAEU_automation
from ZIMU_Code import ZIMU_automation


# initializing SQL connection
tree = ET.parse('server.xml')
root = tree.getroot()
server_tag = root.find('SERVER')
server = server_tag.get('SERVER')
username = server_tag.get('USERNAME')
password = server_tag.get('PASSWORD')
driverSQL = server_tag.get('DRIVERSQL')
Trusted_connection = server_tag.get('Trusted_connection')

conn1 = pyodbc.connect(
    'DRIVER=' + driverSQL + ';SERVER=' + server + ';Trusted_connection=' + Trusted_connection + ';UID=' + username + ';PWD=' + password)
excursor = conn1.cursor()

if __name__ == "__main__":
    config = None
    with open('config_file.json', 'r') as configFile:
        config = json.load(configFile)
    logger.init_logging(config['logging'])
    input_file = config['input-file']
    database = sys.argv[1]
    carrier_list = ['HSUD', 'HAPG', 'ONEX', 'MSCU', 'MAEU', 'ZIMU']
    carrier = ''
    input_db = ''
    for carrier in carrier_list:
        try:
            input_db = excursor.execute("select BL, TEXTO, MEMO, ORIGEM from [" + database + "].[dbo].[MANIFESTO] where tabela_fonte = '"+carrier+"'").fetchall()
            if carrier == 'HSUD' and len(input_db) > 0:
                HSUD_automation(input_db, database, input_file, config, conn1, excursor)
                break
            elif carrier == 'HAPG' and len(input_db) > 0:
                HAPAG_automation(input_db, database, input_file, config, conn1, excursor)
                break
            elif carrier == 'ONEX' and len(input_db) > 0:
                ONEX_automation(input_db, database, input_file, config, conn1, excursor)
                break
            elif carrier == 'MSCU' and len(input_db) > 0:
                MSCU_automation(input_db, database, input_file, config, conn1, excursor)
                break
            elif carrier == 'MAEU' and len(input_db) > 0:
                MAEU_automation(input_db, database, input_file, config, conn1, excursor)
                break
            elif carrier == 'ZIMU' and len(input_db) > 0:
                ZIMU_automation(input_db, database, input_file, config, conn1, excursor)
                break

        except Exception as ex:
            logger.log.error("Error occured", exc_info=ex)
    if len(input_db) > 0:
        print("Database:", database, "Executed successfully for", carrier, "with", len(input_db), "BL's.")
    else:
        print("Database:", database, "does not belong to any of the given Carrier. Please Check the Database.")
