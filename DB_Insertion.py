import logger
from time import *
import pandas as pd
from xlsxwriter import *
from threading import Lock
from concurrent.futures.thread import ThreadPoolExecutor

prev_bl = ' '
this_manifest_id = ' '

pkg_info_dict = {'BALES': 'BLS', 'BARRELS': 'BRL', 'BARS': 'BAR', 'BEAMS': 'BMS', 'BIDONS': 'BDN', 'BBAGS': 'BBG', 'BIG CARTONS': 'BGC',
                 'BINS': 'BIN', 'BLOCKS': 'BLK', 'BOBBINS': 'BOB', 'BOTTELON': 'BOT', 'BOTTLES': 'BTL', 'BOXES': 'BXS', 'BUCKETS': 'BKT',
                 'BULK': 'BKC', 'BULTOS': 'BUL', 'BUNDLES': 'BDL', 'CAGE': 'CAG', 'CANS': 'CA', 'CARTONS': 'CTN', 'CASES': 'CS',
                 'CASKS': 'CSK', 'COILS': 'CLS', 'COLLI': 'COL', 'CRADLES': 'CDL', 'CRATES': 'CRT', 'CYLINDERS': 'CYL', 'DRUMS': 'DRS',
                 'FLASKS': 'FLK', 'FLATS': 'FLT', 'FLEXIBAGS': 'FLB', 'GALLONS': 'GAL', 'GAYLDS': 'GAY', 'IBCS': 'IBC', 'JARS': 'JAR',
                 'JUGS': 'JUG', 'KEGS': 'KGS', 'LIFT VAN': 'LVN', 'LIFTS': 'LFT', 'LITERS': 'LTR', 'LOTS': 'LOT', 'PACKS': 'PKG',
                 'PAILS': 'PLS', 'PAIRS': 'PRS', 'PALLETS': 'PLT', 'PARCELS': 'PCL', 'PIECES': 'PCS', 'PLAST./BAG': 'PLB',
                 'PACKAGES': 'PKG', 'PILES': 'PLS', 'PLATES': 'PLA', 'POLIBAGS': 'PBG', 'RACKS': 'RCK', 'REELS': 'RLS',
                 'ROLLS': 'RLL', 'SACKS': 'SAC', 'PALLET': 'PLT', 'CONTAINER': 'CNT', 'SETS': 'SET', 'SHEETS': 'SHT', 'SKIDS': 'SKD',
                 'SLABS': 'SLB', 'SLING BAG': 'SBG', 'SPOOL': 'SPL', 'TANK': 'THK', 'TIERCES': 'TRC', 'TINS': 'TIN', 'TOTE': 'TOT',
                 'TRAYS': 'TRA', 'UNITS': 'UNT', 'UNKNOWN': 'UNK', 'VAN': 'VAN', 'VOLUMES': 'VOL', 'WOODEN BIN': 'WBI',
                 'BAGS': 'BGS'}


class commodity_report:
    __instance = None

    def __init__(self):
        if commodity_report.__instance is not None:
            raise Exception('Singleton')
        commodity_report.__instance = self
        self.__task_count: int = 0
        self.__lock: Lock = Lock()
        self.__entries = []
        self.__initialized = False

    def init(self, in_file: str, thd_count: int, excursor, conn1, database):
        self.__input_items = pd.read_excel(in_file).values
        self.__executor = ThreadPoolExecutor(thd_count)
        self.__cursor = excursor
        self.__conn = conn1
        self.__database = database
        self.__initialized = True

    @staticmethod
    def instance():
        if commodity_report.__instance is None:
            commodity_report()
        return commodity_report.__instance

    def ncm_check(self, new_ncm):
        ncm_information = ' '
        if str(new_ncm) != '':
            ncm_info = self.__cursor.execute(
                "SELECT CD_MERCADORIA, DC_MERCADORIA FROM [MATRIZ].[dbo].[MERCADORIA] where CD_MERCADORIA = '" + str(
                    new_ncm) + "'").fetchone()
            if ncm_info is None:
                new = new_ncm[:6]
                if str(new) != '':
                    ncm_info = self.__cursor.execute(
                        "SELECT CD_MERCADORIA, DC_MERCADORIA FROM [MATRIZ].[dbo].[MERCADORIA] where CD_MERCADORIA like '" + str(
                            new) + "%'").fetchone()
                    if ncm_info is None:
                        new = new_ncm[:4]
                        if str(new) != '':
                            ncm_info = self.__cursor.execute(
                                "SELECT CD_MERCADORIA, DC_MERCADORIA FROM [MATRIZ].[dbo].[MERCADORIA] where CD_MERCADORIA like '" + str(
                                    new) + "%'").fetchone()
                            if ncm_info is not None:
                                new_ncm = str(ncm_info[0])
                                ncm_information = str(ncm_info[1]).strip(' ')
                            else:
                                new_ncm = '00000000'
                                ncm_information = ' '
                    elif ncm_info is not None:
                        new_ncm = str(ncm_info[0])
                        ncm_information = str(ncm_info[1]).strip(' ')
            elif ncm_info is not None:
                new_ncm = str(ncm_info[0])
                ncm_information = str(ncm_info[1]).strip(' ')
        return ncm_information, new_ncm

    def write_entry(self, row_index: int, line: list):
        global prev_bl, this_manifest_id
        nitem = ()
        pkg, qnty = '', ''
        line_bl = line[0]
        ncm_list = line[1]
        weight = str(line[2])
        cbm = str(line[3])
        pkg_list = str(line[4])
        qnty_list = str(line[5])
        flag = 0
        ncm_list_updated = []
        if weight != 'nan' and cbm != 'nan' and pkg_list != 'nan' and qnty_list != 'nan':
            pkg_listed = pkg_list.strip("['").strip("']").split(',')
            if len(pkg_listed) == 1:
                pkg = str(pkg_listed).strip("['").strip("']").strip("'")
            else:
                pkg = str(pkg_listed[0]).strip("'")
            qnty_listed = qnty_list.strip("['").strip("']").split(',')
            if len(qnty_listed) == 1:
                qnty = str(qnty_listed).strip("['").strip("']").strip("'")
            else:
                qnty = str(qnty_listed[0]).strip("'")
            flag = 1
        ncm_list = str(ncm_list).replace('97970000', '99960000').replace('99999911', '99960000').replace('98980000', '99960000')
        if str(ncm_list).__contains__("[]") or str(ncm_list).__contains__("['']") or str(ncm_list).__contains__('nan'):
            ncm_list = ['00000000']
        ncm = str(ncm_list).replace(',', '').replace("'", "").strip("[").strip("]").split(' ')

        ncm = list(set(ncm))                   # getting unique NCM's

        # check for NCM is present in Martiz table
        popping_ncm = []
        for i in range(len(ncm)):
            ncm_info = self.__cursor.execute(
                "SELECT DC_MERCADORIA FROM [MATRIZ].[dbo].[MERCADORIA] where CD_MERCADORIA = '" + str(
                    ncm[i]) + "'").fetchone()
            if ncm_info is None:
                new = str(ncm[i])[:6]
                ncm_info = self.__cursor.execute(
                    "SELECT DC_MERCADORIA FROM [MATRIZ].[dbo].[MERCADORIA] where CD_MERCADORIA like '" + str(
                        new) + "%'").fetchone()
                if ncm_info is None:
                    new = str(ncm[i])[:4]
                    ncm_info = self.__cursor.execute(
                        "SELECT DC_MERCADORIA FROM [MATRIZ].[dbo].[MERCADORIA] where CD_MERCADORIA like '" + str(
                            new) + "%'").fetchone()
                    if ncm_info is None:
                        popping_ncm.append(ncm[i])
            continue
        if len(ncm) > 1:
            ncm = [e for e in ncm if e not in popping_ncm]

        # cross check for NCM is present in Martiz Table and fetching ncm_information
        updated_ncm = []
        if len(ncm) == 1:
            ncm_info, new_ncm = self.ncm_check(ncm[0])
            updated_ncm.append(new_ncm)
        else:
            for each_ncm in ncm:
                ncm_info, new_ncm = self.ncm_check(each_ncm)
                if new_ncm == '00000000':
                    continue
                updated_ncm.append(new_ncm)
        ncm = updated_ncm
        ncm_list_updated = updated_ncm    # Considering this for Various NCM table

        # HS6 Checking
        i, count = 0, 0
        while i < len(ncm) - 1:
            if ncm[i] == ncm[i + 1] or ncm[i][:6] == ncm[i + 1][:6]:
                count += 1
            else:
                break
            i += 1
            continue
        ncm_update = []
        if count == len(ncm) - 1:
            ncm_update.append(ncm[i])
            ncm = ncm_update

        # Principle NCM checking
        if len(ncm) > 1:
            principle_ncm = ncm[0][:2]
            counter = 0
            dummy_list = []
            for each_ncm in ncm:
                if each_ncm[:2] == principle_ncm:
                    counter += 1
            if counter == len(ncm):
                dummy_list.append(ncm[0])
                ncm = dummy_list

        if len(ncm) == 1:
            new_ncm = str(ncm).strip("['").strip("']")
        elif len(ncm) > 1:
            new_ncm = '00330000'
        else:
            new_ncm = '00000000'

        # fetching Manif_Desc column
        ncm_info = self.__cursor.execute(
                "SELECT DC_MERCADORIA FROM [MATRIZ].[dbo].[MERCADORIA] where CD_MERCADORIA = '" + str(new_ncm) + "'").fetchone()
        ncm_information = str(ncm_info[0]).strip(' ')

        if qnty == '' or qnty == ' ':
            qnty = 0
        try:
            qnty = int(float(qnty))
        except:
            qnty = 0

        # merge package info for subitems
        pck_desc_sub = ''
        for pkg_key, pkg_value in pkg_info_dict.items():
            if pkg_value == pkg:
                pck_desc_sub = str(qnty) + ' ' + pkg_key + ' ' + 'OF' + ' '

        if line_bl != prev_bl:              # checking for not subitem BL
            nitem = (1,)

            # fetching package info from Port sequencing
            pkg_information = self.__cursor.execute(
                "SELECT DESC_MANIF_COMPL FROM [" + self.__database + "].[dbo].[MANIFESTO] WHERE BL = '" + str(
                    line_bl) + "'").fetchone()
            pkg_info = str(pkg_information[0])
            pkg_info_desc = pkg_info + ' '

            if flag == 1:                                                      # Updating first subitem
                query1 = '''UPDATE '''+self.__database+'''.dbo.MANIFESTO 
                    SET DESC_MANIF_COMPL= ? , QTY= ?, WTKG= ?, PCK= ?, CARGO_VOLUME= ? , MANIF_DESC= ?, 
                    COD_MANIF_DESC= ? , CONFERENTE_MERC= 'Automatic', CONFERENTE_WTKG= 'Automatic', 
                    CONFERENTE_QTY= 'Automatic', CONFERENTE_PCK= 'Automatic', CONFERENTE_COPY_PAST= 'Automatic' WHERE BL = ? '''
                try:
                    values1 = [pkg_info_desc, qnty, float(weight), str(pkg), str(cbm), str(ncm_information), str(new_ncm), str(line_bl)]
                    self.__cursor.execute(query1, values1)
                    #logger.log.debug("write_entry: Updating manifesto DB")
                except:
                    #logger.log.error("write_entry: While Updating manifesto DB")
                    values1 = [pkg_info, 0, 0.00, '', '', '', '', str(line_bl)]
                    self.__cursor.execute(query1, values1)

            elif flag == 0:                                                     # Updating for simple and various
                query1 = '''UPDATE ''' + self.__database + '''.dbo.MANIFESTO 
                                SET DESC_MANIF_COMPL= ?, MANIF_DESC= ?, COD_MANIF_DESC= ?, CONFERENTE_MERC= 'Automatic',
                                CONFERENTE_WTKG= 'Automatic', CONFERENTE_QTY= 'Automatic', CONFERENTE_PCK= 'Automatic',
                                CONFERENTE_COPY_PAST= 'Automatic' WHERE BL = ? '''
                try:
                    values1 = [pkg_info_desc, str(ncm_information), str(new_ncm), str(line_bl)]
                    self.__cursor.execute(query1, values1)
                    #logger.log.debug("write_entry: Updating manifesto DB")
                except:
                    #logger.log.error("write_entry: While Updating manifesto DB")
                    values1 = [pkg_info, '', '', str(line_bl)]
                    self.__cursor.execute(query1, values1)

            this_manifest_id = self.__cursor.execute(
                "SELECT ID_MANIFESTO FROM ["+self.__database+"].[dbo].[MANIFESTO] WHERE BL = '" + str(line_bl) + "'").fetchone()

        elif line_bl == prev_bl:                                              # Inserting for subitems
            max_id = self.__cursor.execute("SELECT max(ID_MANIFESTO)+1 FROM ["+self.__database+"].[dbo].[MANIFESTO]").fetchone()
            nitem = self.__cursor.execute("SELECT max(NITEM)+1 FROM ["+self.__database+"].[dbo].[MANIFESTO] WHERE BL = '" + line_bl + "'").fetchone()
            query2 = '''INSERT INTO '''+self.__database+'''.dbo.MANIFESTO SELECT ?, [CD_MOV], [ORIGEM], [TABELA_FONTE], 
            [TABELA_ORIGEM], [TABELA_CNT], [BL], [OPE], [VSL], [CARRIER], [VOY], [SH], [SHCORR], [SHDEITRAM], [SH_PAIS], 
            [SHCODE_BKP], [SHCODE_CLEANED], [CO], [CO_PAIS], [COCORR], [CODEITRAM], [COCODE_BKP], [COCODE_CLEANED], [FW], 
            [FWCORR], [FWDEITRAM], [F_FW], [F_SH], [F_CO], [F_NF1], [F_NF2], [POR], [POL], [POD], [DEST], [RC], [DL], 
            [ID_ESCALA], [PIN], [PORT], [PDIR], [PEX], [PIN2], [NF1], [NF1_PAIS], [NF1CORR], [NF1DEITRAM], [NF2], [NF3], 
            [POLDATE], [PODDATE], [MOD], ?, ?, ?, ?, [CNT], [TS1], [TS2], [TS3], [FWCODE], [SHCODE], [COCODE], [NF1CODE], 
            [NF2CODE], [WTKG_TOTAL], [DT_MANIF], [MEMO], [NUM_CONTAINER], [MEMO1], [TEXTO], [MARKS], [TRADERS], [YYYYMM], 
            [CONFERENTE_SH], [CONFERENTE_CO], [CONFERENTE_FW], [CONFERENTE_NF1], 'Automatic', [CONFERENTE_EXCL], 'Automatic', 
            'Automatic', 'Automatic', [CONFERENTE_CNT], 'Automatic', [INCLUSAO], [VARIOS_ITENS], [EXCLUIR], [DT_EXCLUSAO], 
            [DT_IMPORTACAO], [ARQUIVOIMPORTADO], [QTD20], [QTD40], [QT_CONT20], [QT_CONT40], [CNT_PERC], [DUPLICIDADE], 
            [NCM_BODYTEXT], ?, ?, [DESC_MANIF_DESC], ?, [TP_BL], [SERVICO], [DATA_CORRECAO_MERCADORIA], [TRADER_SH_CHK], 
            [TRADER_CO_CHK], [TRADER_FW_CHK], [TRADER_NF1_CHK], [EDI_WEEK], [SUBITEM_CHK], [ADD_ITENS], [SEM_FW], [TEU], 
            [Date_vessel], [CNPJ], [DOUBT_COMMENTARY], [USER_COMPLETE], [Varios_Ncms], [QC_COMPLETE], [DUPLI], [ARR_DATE], 
            [ID_ESCALA_POM], [VSL_POM], [VOY_POM], [POM], [FEED], [EMPTY], [IDLOCATED], [DIGITADOR_COD_MANIF_DESC], [FW_PAIS], 
            [wtkg_original], [sh_is_hard], [co_is_hard], [code_is_hard], [desc_manif_compl_is_hard], [fw_is_hard], [nf1_is_hard], 
            [qty_is_hard], [pck_is_hard], [wtkg_is_hard], [cnt_is_hard], [ID_MOVPORT_PRIM], [TP_CARGO], [REAL_OWNER], 
            [CONFERENTE_REAL], [NCMCOUNT] , [FLAGFW], [FLAGSH], [FLAGCON], [FLAGNF1], [FLAGCODE], [FLAGDESC], [EmpInitial], 
            [EmpReassign], [EmpAssDate], [EmpReAssignDate], [ActvUserFW], [ActvUserSH], [ActvUserCON], [ActvUserNF1], 
            [ActvUserCODE], [ActvUserDESC], [Comm], [CONFERENTE_SH_QC], [CONFERENTE_CO_QC], [CONFERENTE_FW_QC], [CONFERENTE_NF1_QC], 
            [CONFERENTE_MERC_QC], [CONFERENTE_EXCL_QC], [CONFERENTE_WTKG_QC], [CONFERENTE_QTY_QC], [CONFERENTE_PCK_QC], 
            [CONFERENTE_CNT_QC], [CONFERENTE_COPY_PAST_QC], [QC_Comment_SH], [QC_Comment_CO], [QC_Comment_FW], [QC_Comment_NF1], 
            [QC_Comment_MERC], [QC_Comment_WTKG], [QC_Comment_QTY], [QC_Comment_PCK], [QC_Comment_CNT], [QC_Comment_COPY_PAST], 
            [OPE_Comment_SH], [OPE_Comment_CO], [OPE_Comment_FW], [OPE_Comment_NF1], [OPE_Comment_MERC], [OPE_Comment_WTKG], 
            [OPE_Comment_QTY], [OPE_Comment_PCK], [OPE_Comment_CNT], [OPE_Comment_COPY_PAST], [SH_Status], [CO_Status], 
            [FW_Status], [NF1_Status], [MERC_Status], [WTKG_Status], [QTY_Status], [CNT_Status], [PCK_Status], [DESC_Status], 
            [SH_Corrected_date], [CO_Corrected_date], [FW_Corrected_date], [NF1_Corrected_date], [MERC_Corrected_date],
            [WTKG_Corrected_date], [QTY_Corrected_date], [CNT_Corrected_date], [PCK_Corrected_date], [DESC_Corrected_date],
            [Freight_Captured], [Freight_Analysed], [NCM_DATAMAR], [NCM_DATAMAR_DESC], [PRODUCT_DATAMAR1], [PRODUCT_DATAMAR2],
            [PRODUCT_DATAMAR3], ?, [CARGO_TOTAL], [CARGO_ORIGINAL], [CONFERENTE_CARGO_VOLUME], [CONFERENTE_NCM_DATAMAR], 
            [CONFERENTE_Freight_INFO], [CONFERENTE_PR_DATAMAR1], [CONFERENTE_PR_DATAMAR2], [CONFERENTE_PR_DATAMAR3], 
            ' ', ' ', ' ', [IMO_CLASS_CAPTURED], ' ', ' ', [PCK_DESC], [PRODUCT_DATAMAR4], 
            [CONFERENTE_PR_DATAMAR4], [PRODUCT_DATAMAR5], [CONFERENTE_PR_DATAMAR5], [PRIM] FROM 
            '''+self.__database+'''.dbo.MANIFESTO WHERE ID_MANIFESTO = ? '''
            try:
                values2 = [max_id[0], nitem[0], qnty, float(weight), str(pkg), str(ncm_information), str(new_ncm),
                           str(pck_desc_sub), str(cbm), this_manifest_id[0]]
                self.__cursor.execute(query2, values2)
                #logger.log.debug("write_entry: Inserting into manifesto DB")
            except:
                #logger.log.error("write_entry: While Inserting manifesto DB")
                values2 = [max_id[0], nitem[0], 0, 0.00, '', '', '', '', '', this_manifest_id[0]]
                self.__cursor.execute(query2, values2)
        prev_bl = line_bl

        # To link Manifesto and Varios_itens table
        manifesto_db = self.__cursor.execute(
                "SELECT ID_MANIFESTO FROM ["+self.__database+"].[dbo].[MANIFESTO] where bl= '"+str(line_bl)+"' and nitem='"+str(nitem[0])+"'").fetchone()
        manifesto_id = str(manifesto_db[0])

        # Adding multiple NCM to Varios_Itens table
        new_ncm_list = str(ncm_list_updated).split(' ')
        if len(new_ncm_list) > 1:
            newlist = []
            try:
                for nc in new_ncm_list:
                    new_ncm = str(nc).replace(']', '').replace('[', '').replace('"', '').replace("'", '')
                    if new_ncm not in newlist:
                        if str(new_ncm) != '':
                            ncm_information, new_ncm = self.ncm_check(new_ncm)
                            if new_ncm == '00000000':
                                continue
                            else:
                                self.__cursor.execute("INSERT INTO " + self.__database + ".dbo.Varios_Itens(ID_MANIFESTO, CD_MERCADORIA) "
                                                                       "VALUES ('" + str(manifesto_id) + "','" + str(new_ncm) + "')")
                            newlist.append(new_ncm)
                #logger.log.debug("write_entry: Inserting varios_itens DB")
            except Exception as ex:
                logger.log.error("write_entry: While inserting into varios_itens DB ", ex)
        self.__conn.commit()
        self.__task_count = self.__task_count - 1

    def process_line(self, line, index: int):
        self.__index = index
        self.__line = line
        cdt_report: commodity_report = commodity_report.instance()
        self.__lock.acquire()
        self.__task_count = self.__task_count + 1
        cdt_report.write_entry(self.__index, self.__line)
        self.__lock.release()

    def generate(self):
        index = 1
        for line in self.__input_items:
            #logger.log.debug(str(index) + " BL- " + str(line[0]))
            self.process_line(line, index)
            index = index + 1
        while True:
            try:
                self.__lock.acquire()
                task_count = self.__task_count
                self.__lock.release()
                if task_count == 0:
                    break
                sleep(10)
            except Exception as ex:
                #logger.log.error('generate: lock as occured', ex)
                if self.__lock.locked():
                    self.__lock.release()
            finally:
                self.__out_file.close()


def insertion_main(input_excel, excursor, conn1, database, config):
    try:
        #logger.log.debug('main: Entry Search Began')
        report: commodity_report = commodity_report.instance()
        th_count = config['threads-count']
        #logger.log.debug('main: all modules initialized')
    except Exception as ex:
        #logger.log.error("main: In Main Function - loading files", ex)
        return 1
    report.init(input_excel, th_count, excursor, conn1, database)
    try:
        report.generate()
    except Exception as ex:
        logger.log.error("main: In report generate", ex)
    # input_excel deleting
    return 1
